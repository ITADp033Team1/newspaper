﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SoftServe.Modules.NewsPaperExtensions.Contracts;
using SoftServe.Modules.NewsPaperExtensions.Models;
using System.Xml.Serialization;
using System.IO;
using System.Linq;
using SoftServe.Modules.NewsPaperExtensions.Abstractions;
using System.Runtime.Serialization;

namespace SoftServe.Testing.NewsPaperExtensionsTesting
{
    /// <summary>
    /// Unit-testing for AdminControllerTestClass
    /// </summary>
    [TestClass]
    public class AdminControllerTestClass
    {


        string pathTemplate = "../../Stubs/{0}TestData.xml";

        [TestMethod]
        public void AdminController_DeleteUserProfile()
        {
            UserProfile user = new UserProfile()
            {
                Id = Guid.Parse("{b86880d0-3ffa-4a65-868f-a46c025bc713}")
            };
            if (user.Id != null)
            {
                using (var controller = SoftServe.Modules.NewsPaperExtensions.LogicContainer.Instance.GetService<IAdminContract>())
                {
                    controller.DeleteUserProfile(user.Id);
                    var test = controller.GetUserProfile(user.Id);
                    Assert.IsNull(test);
                }
            }
            
        }

        [TestMethod]
        public void AdminController_SetUserProfileState()
        {
            UserProfile user = new UserProfile()
            {
                Id = new Guid("b86880d0-3ffa-4a65-868f-a46c025bc713")
            };

            eUserAccountState status = eUserAccountState.Active;
            if (user.Id != null)
            {
                using (var controller = SoftServe.Modules.NewsPaperExtensions.LogicContainer.Instance.GetService<IAdminContract>())
                {
                    controller.SetUserProfileState(user.Id, eUserAccountState.Active);
                    user = controller.GetUserProfile(user.Id) as UserProfile;
                    Assert.IsNotNull(user);
                    Assert.IsTrue(user.UserStatus == status);
                }
            }

        }

        [TestMethod]
        public void AdminController_GetUserProfile()
        {
            UserProfile user = new UserProfile()
            {
                Id = new Guid("b86880d0-3ffa-4a65-868f-a46c025bc713")
            };

            if (user.Id != null)
            {
                using (var controller = SoftServe.Modules.NewsPaperExtensions.LogicContainer.Instance.GetService<IAdminContract>())
                {
                    var profile = controller.GetUserProfile(user.Id);
                    Assert.IsNotNull(profile);
                }
            }
        }
        [TestMethod]
        public void AdminController_ResetUserPassword()
        {
            Guid id = new Guid("b86880d0-3ffa-4a65-868f-a46c025bc713");
            //using (var repo = SoftServe.Modules.NewsPaperExtensions.LogicContainer.Instance.GetService<Ocorola.Module.Infrastructure.IRepository<UserProfile>>() )
            //{
            //    var data = new List<UserProfile>( repo.GetQuery().Where(user => user.Id == id));

            //    for (int i = 0; i < data.Count-1; i++)
            //    {
            //        repo.Remove(data[i]);
            //    }
            //    data = new List<UserProfile>(repo.GetQuery().Where(user => user.Id == id));

                //repo.Add(new UserProfile()
                //{
                //    Id = id,
                //    Email = "johnsmith@gmail.com",
                //    FIO = "John",
                //    Login = "johnsmith",
                //    Password = "ihavestolenyourcookiesinyourchildhood",
                //    Picture = "1.jpg",
                //    Tags = "Cookies",
                //    UserStatus = eUserAccountState.Active
                //});
            //}
            using (var controller = SoftServe.Modules.NewsPaperExtensions.LogicContainer.Instance.GetService<IAdminContract>())
            {
                var newpassword = controller.ResetUserPassword(id);
                var profile = controller.GetUserProfile(id);
                Assert.AreEqual(newpassword, profile.Password);
            }
        }
        [TestMethod]
        public void AdminController_GetUsers()
        {
            var serializer = new DataContractSerializer(typeof(List<UserProfile>));
            using (var stream = new FileStream(string.Format(pathTemplate, "UserProfile"), FileMode.Open, FileAccess.Read))
            {
                var data = serializer.ReadObject(stream) as List<UserProfile>;
                using (var controller = SoftServe.Modules.NewsPaperExtensions.LogicContainer.Instance.GetService<IAdminContract>())
                {
                    var assertData = controller.GetUsers() as List<UserProfile>;
                    Assert.AreEqual(data.Count, assertData.Count);
                }
            }
        }

        /// <summary>
        /// Create request to become moderator, aprove it
        /// </summary>
        [TestMethod]
        public void AdminController_ApproveModeratorAccount()
        {
            string login = "John Smith";
            using (var controller = SoftServe.Modules.NewsPaperExtensions.LogicContainer.Instance.GetService<IAdminContract>())
            {
                controller.ApproveModeratorAccount(login);
            }
        }

        /// <summary>
        /// Create request to become moderator, decline it
        /// </summary>
        [TestMethod]
        public void AdminController_DeclineModeratorAccount()
        {
            string login = "John Smith";
            using (var controller = SoftServe.Modules.NewsPaperExtensions.LogicContainer.Instance.GetService<IAdminContract>())
            {
                controller.DeclineModeratorAccount(login);
            }
        }
    }
}
