﻿using SoftServe.Modules.NewsPaperExtensions.Controllers.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SoftServe.Modules.NewsPaperExtensions.Models;
using System.Collections.Generic;
using System.Linq;
using Ocorola.Entity.Infrastructure;

namespace SoftServe.Testing.NewsPaperExtensionsTesting.RepositoriesTesting
{
    
    
    /// <summary>
    ///This is a test class for AdminProfileRepositoryTest and is intended
    ///to contain all AdminProfileRepositoryTest Unit Tests
    ///</summary>
    [TestClass()]
    public class AdminProfileRepositoryTest : AbstractrepositoryTestClass<AdminProfile>
    {
        [TestInitialize]
        public void Init()
        {
            foreach (var item in testItems)
            {
                mRepository.Add(item);
            }
        }

        [TestCleanup]
        public void Clean()
        {
            foreach (var item in mRepository)
            {
                mRepository.Remove(item);
            }
        }

        /// <summary>
        ///A test for Add
        ///</summary>
        [TestMethod()]
        public void AddTest()
        {
            mRepository.Add(testItem);
            Assert.IsNotNull(mRepository.Item(testItem.Id));
        }

        /// <summary>
        ///A test for Create
        ///</summary>
        [TestMethod()]
        public override void CreateTest()
        {
            base.CreateTest();
        }

        /// <summary>
        ///A test for Enumerate
        ///</summary>
        [TestMethod()]
        public override void EnumerateTest()
        {
            base.EnumerateTest();
        }

        /// <summary>
        ///A test for GetEnumerator
        ///</summary>
        [TestMethod()]
        public override void GetEnumeratorTest()
        {
            base.GetEnumeratorTest();
        }

        /// <summary>
        ///A test for GetQuery
        ///</summary>
        [TestMethod()]
        public override void GetQueryTest()
        {
            base.GetQueryTest();
        }

        /// <summary>
        ///A test for Item
        ///</summary>
        [TestMethod()]
        public void ItemTest()
        {
            var actual = mRepository.Item(testItems[0].Id);
            Assert.IsTrue(actual != null & actual.IsKeyEquals(testItems[0].Id));
        }

        /// <summary>
        ///A test for Remove
        ///</summary>
        [TestMethod()]
        public void RemoveTest()
        {
            mRepository.Remove(testItems[1]);
            Assert.IsTrue(mRepository.Item(testItems[1].Id) == null);
        }

        /// <summary>
        ///A test for Update
        ///</summary>
        [TestMethod()]
        public void UpdateTest()
        {
            var itemToChange = (AdminProfile)mRepository.Item(testItems[2].Id);
            itemToChange.Email = "NewAdmin@Mail.org";
            mRepository.Update(itemToChange);
            Assert.IsTrue(((AdminProfile)mRepository
                .Item(testItems[2].Id)).Email == itemToChange.Email);
        }

        #region Test Data
        private static AdminProfile testItem = new AdminProfile()
        {
            Id = Guid.NewGuid(),
            Email = "admin5@mail.com",
            Login = "admin5",
            Password = "qwerty",
            Picture = "C:\\pic5.jpg",
            Tags = "Tag0 Tag1 Tag2",            
        };

        private static List<AdminProfile> testItems = new List<AdminProfile>()
        {
            new AdminProfile() 
                { 
                    Id = Guid.NewGuid(),
                    Email = "admin0@mail.com",
                    Login = "admin0",
                    Password = "qwerty",
                    Picture = "C:\\pic.jpg",
                    Tags = "Tag0 Tag1 Tag2",                    
                },
                new AdminProfile() 
                { 
                    Id = Guid.NewGuid(),
                    Email = "admin1@mail.com",
                    Login = "admin1",
                    Password = "qwerty",
                    Picture = "C:\\pic.jpg",
                    Tags = "Tag0 Tag1 Tag2",                    
                },
                new AdminProfile() 
                { 
                    Id = Guid.NewGuid(),
                    Email = "admin2@mail.com",
                    Login = "admin2",
                    Password = "qwerty",
                    Picture = "C:\\pic.jpg",
                    Tags = "Tag0 Tag1 Tag2",                    
                },
                new AdminProfile() 
                { 
                    Id = Guid.NewGuid(),
                    Email = "admin3@mail.com",
                    Login = "admin3",
                    Password = "qwerty",
                    Picture = "C:\\pic.jpg",
                    Tags = "Tag0 Tag1 Tag2",      
                    
                }
        };
        #endregion
    }
}
