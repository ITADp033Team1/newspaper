﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ocorola.Module.Infrastructure.Models;
using Ocorola.Module.Infrastructure;
using Ocorola.Module.Infrastructure.Abstractions;
using SoftServe.Modules.NewsPaperExtensions;
using System.Collections.Generic;
using System.Linq;

namespace SoftServe.Testing.NewsPaperExtensionsTesting.RepositoriesTesting
{
    /// <summary>
    /// Respesents base class for repositories test classes
    /// </summary>
    /// <typeparam name="DModel">Type of model</typeparam>
    [TestClass()]
    public class AbstractrepositoryTestClass<DModel>
        where DModel : DomainModelBase, new()
    {

        [TestCleanup]
        public void Dispose()
        {
            mRepository.Dispose();
        }

        [TestInitialize]
        public void Initialize()
        {
            mRepository = LogicContainer.Instance.GetService<IRepository<DModel>>();
        }

        [TestMethod]
        public virtual void Create_Test()
        {
            Assert.IsNotNull(mRepository.Create());
        }

        [TestMethod]
        public virtual void Enumerate_Test()
        {
            Assert.IsNotNull(mRepository.Enumerate());
        }

        [TestMethod()]
        public virtual void GetQuery_Test()
        {
            Assert.IsNotNull(mRepository.GetQuery());
        }

        [TestMethod]
        public virtual void GetEnumerator_Test()
        {
            Assert.IsNotNull(mRepository.GetEnumerator());
        }

        protected IRepository<DModel> mRepository = default(IRepository<DModel>);
    }
}
