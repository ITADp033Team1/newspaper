﻿using SoftServe.Modules.NewsPaperExtensions.Controllers.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SoftServe.Modules.NewsPaperExtensions.Models;
using System.Collections.Generic;
using System.Linq;
using Ocorola.Entity.Infrastructure;

namespace SoftServe.Testing.NewsPaperExtensionsTesting.RepositoriesTesting
{
    
    
    /// <summary>
    ///This is a test class for UserProfileRepository
    ///</summary>
    [TestClass()]
    public class UserProfileRepositoryTest: AbstractrepositoryTestClass<UserProfile>
    {


        /// <summary>
        ///A test for Add
        ///</summary>
        [TestMethod()]
        public void Add_Test()
        {
            if (mRepository.Item(testItemId) != null)
            {
                try
                {
                    mRepository.Add(testItem);
                }
                catch (InvalidOperationException ex)
                {
                    Assert.IsNotNull(ex);
                }
            }
            else
            {
                mRepository.Add(testItem);
                Assert.IsNotNull(mRepository.Item(testItemId));
            }
        }

        /// <summary>
        ///A test for Create
        ///</summary>
        [TestMethod()]
        public override void Create_Test()
        {
            base.Create_Test();
        }

        /// <summary>
        ///A test for Enumerate
        ///</summary>
        [TestMethod()]
        public override void Enumerate_Test()
        {
            base.Enumerate_Test();
        }

        /// <summary>
        ///A test for GetEnumerator
        ///</summary>
        [TestMethod()]
        public override void GetEnumerator_Test()
        {
            base.GetEnumerator_Test();
        }

        /// <summary>
        ///A test for GetQuery
        ///</summary>
        [TestMethod()]
        public override void GetQuery_Test()
        {
            base.GetQuery_Test();
        }

        /// <summary>
        ///A test for Item
        ///</summary>
        //[TestMethod()]
        public void Item_Test()
        {
            object key = new Guid("0eb2026e-993f-4dcb-b785-95c257eae365");
            var actual = mRepository.Item(key);
            Assert.IsTrue(actual != null & actual.IsKeyEquals(key));
        }

        /// <summary>
        ///A test for Remove
        ///</summary>
       // [TestMethod()]
        public void Remove_Test()
        {
            mRepository.Remove(testItem);
            Assert.IsTrue(mRepository.Item(testItemId) == null);
        }

        /// <summary>
        ///A test for Update
        ///</summary>
        //[TestMethod()]
        public void Update_Test()
        {
            testItem.Password = "qwerty123";
            mRepository.Update(testItem);
            Assert.IsTrue(((UserProfile)mRepository.Item(testItemId)).Password == testItem.Password);
        }

        Guid testItemId = new Guid("00000000000000000000000000000001");
        UserProfile testItem = new UserProfile()
        {
            Id = new Guid("00000000000000000000000000000001"),
            Email = "User@use.us",
            Login = "Coolest user",
            Password = "qwerty",
            Picture = "C:\\PIC.png",
            Tags = "Kill",
        };
    }
}
