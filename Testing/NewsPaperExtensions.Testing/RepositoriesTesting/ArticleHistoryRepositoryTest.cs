﻿using SoftServe.Modules.NewsPaperExtensions.Controllers.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SoftServe.Modules.NewsPaperExtensions.Models;
using System.Collections.Generic;
using System.Linq;
using Ocorola.Entity.Infrastructure;
using Ocorola.Module.Infrastructure.Models;

namespace SoftServe.Testing.NewsPaperExtensionsTesting.RepositoriesTesting
{
    
    
    /// <summary>
    ///This is a test class for ArticleHistoryRepository
    ///</summary>
    [TestClass()]
    public class ArticleHistoryRepositoryTest: AbstractrepositoryTestClass<ArticleHistory>
    {
        /// <summary>
        ///A test for Add
        ///</summary>
        [TestMethod()]
        public void Add_Test()
        {
            var id = Guid.NewGuid();
            ArticleHistory item = new ArticleHistory() { ArticleId = id };
            mRepository.Add(item);
            Assert.IsNotNull(mRepository.Item(id));
        }

        /// <summary>
        ///A test for Create
        ///</summary>
        [TestMethod()]
        public override void Create_Test()
        {
            base.Create_Test();
        }

        /// <summary>
        ///A test for Enumerate
        ///</summary>
        [TestMethod()]
        public override void Enumerate_Test()
        {
            base.Enumerate_Test();
        }

        /// <summary>
        ///A test for GetEnumerator
        ///</summary>
        [TestMethod()]
        public override void GetEnumerator_Test()
        {
            base.GetEnumerator_Test();
        }

        /// <summary>
        ///A test for GetQuery
        ///</summary>
        [TestMethod()]
        public override void GetQuery_Test()
        {
            base.GetQuery_Test();
        }

        /// <summary>
        ///A test for Item
        ///</summary>
        //[TestMethod()]
        public void ItemTest()
        {
            object key = new Guid("00000000000000000000000000000001");
            var actual = mRepository.Item(key);
            Assert.IsTrue(actual != null & actual.IsKeyEquals(key));
        }

        /// <summary>
        ///A test for Remove
        ///</summary>
        [TestMethod()]
        public void Remove_Test()
        {
            Guid key = new Guid("00000000000000000000000000000001");
            var entity = new ArticleHistory() { ArticleId = key };
            mRepository.Remove(entity);
            Assert.IsTrue(mRepository.Item(key) == null);
        }

        /// <summary>
        ///A test for Update
        ///</summary>
        //[TestMethod()]
        public void Update_Test()
        {
            Guid key = new Guid("00000000000000000000000000000001");
            var itemToChange = (ArticleHistory)mRepository.Item(key);
            var list = (List<Article>)itemToChange.History;
            int expected = list.Count + 1;
            list.Add(new Article() { Id = key, Content = "Brand new content", Updated = DateTime.Now });
            itemToChange.History = list;
            mRepository.Update(itemToChange);
            int actual = ((ArticleHistory)mRepository.Item(key)).History.ToList<DomainModelBase>().Count;
            Assert.AreEqual(expected, actual);
        }
    }
}
