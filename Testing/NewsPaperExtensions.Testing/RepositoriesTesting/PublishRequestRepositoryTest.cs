﻿using SoftServe.Modules.NewsPaperExtensions.Controllers.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SoftServe.Modules.NewsPaperExtensions.Models;
using System.Collections.Generic;
using System.Linq;
using Ocorola.Entity.Infrastructure;

namespace SoftServe.Testing.NewsPaperExtensionsTesting.RepositoriesTesting
{
    
    
    /// <summary>
    ///This is a test class for PublishRequestRepository
    ///</summary>
    [TestClass()]
    public class PublishRequestRepositoryTest : AbstractrepositoryTestClass<PublishRequest>
    {


        /// <summary>
        ///A test for Add
        ///</summary>
        [TestMethod()]
        public void AddTest()
        {
            if (mRepository.Item(testItemId) != null)
            {
                try
                {
                    mRepository.Add(testItem);
                }
                catch (InvalidOperationException ex)
                {
                    Assert.IsNotNull(ex);
                }
            }
            else
            {
                mRepository.Add(testItem);
                Assert.IsNotNull(mRepository.Item(testItemId));
            }
        }

        /// <summary>
        ///A test for Create
        ///</summary>
        [TestMethod()]
        public override void Create_Test()
        {
            base.Create_Test();
        }

        /// <summary>
        ///A test for Enumerate
        ///</summary>
        [TestMethod()]
        public override void Enumerate_Test()
        {
            base.Enumerate_Test();
        }

        /// <summary>
        ///A test for GetEnumerator
        ///</summary>
        [TestMethod()]
        public override void GetEnumerator_Test()
        {
            base.GetEnumerator_Test();
        }

        /// <summary>
        ///A test for GetQuery
        ///</summary>
        [TestMethod()]
        public override void GetQuery_Test()
        {
            base.GetQuery_Test();
        }

        /// <summary>
        ///A test for Item
        ///</summary>
        //[TestMethod()]
        public void Item_Test()
        {
            object key = new Guid("{B86880D0-3FFA-4A65-868F-A46C025BC713}");
            var actual = mRepository.Item(key);
            Assert.IsTrue(actual != null & actual.IsKeyEquals(key));
        }

        /// <summary>
        ///A test for Remove
        ///</summary>
       // [TestMethod()]
        public void Remove_Test()
        {
            mRepository.Remove(testItem);
            Assert.IsTrue(mRepository.Item(testItemId) == null);
        }

        /// <summary>
        ///A test for Update
        ///</summary>
        [TestMethod()]
        public void Update_Test()
        {
            testItem.Header = "New Header";
            mRepository.Update(testItem);
            Assert.IsTrue(((PublishRequest)mRepository.Item(testItemId)).Header == testItem.Header);
        }

        Guid testItemId = new Guid("00000000000000000000000000000001");
        PublishRequest testItem = new PublishRequest()
        {
            Id = new Guid("00000000000000000000000000000001"),
            ArticleInfo = new KeyValuePair<Guid,string>(new Guid("5a948975-9451-4a55-8718-e2faee49c565"), "Article"),
            Date = DateTime.Now,
            Header = "HEEEEAD",
            PublisherInfo = new KeyValuePair<Guid, string>(new Guid("4992e5f0-3682-4859-a2b4-018c373250b3"), "David and Catherine Birnie"),
            Tags = "Kill",
        };
    }
}
