﻿using SoftServe.Modules.NewsPaperExtensions.Controllers.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SoftServe.Modules.NewsPaperExtensions.Models;
using System.Collections.Generic;
using System.Linq;
using Ocorola.Entity.Infrastructure;

namespace SoftServe.Testing.NewsPaperExtensionsTesting.RepositoriesTesting
{
    
    
    /// <summary>
    ///This is a test class for PublishResponseRepository
    ///</summary>
    [TestClass()]
    public class PublishResponseRepositoryTest: AbstractrepositoryTestClass<PublishResponce>
    {

        /// <summary>
        ///A test for Add
        ///</summary>
        [TestMethod()]
        public void AddTest()
        {
            if (mRepository.Item(testItemId) != null)
            {
                try
                {
                    mRepository.Add(testItem);
                }
                catch (InvalidOperationException ex)
                {
                    Assert.IsNotNull(ex);
                }
            }
            else
            {
                mRepository.Add(testItem);
                Assert.IsNotNull(mRepository.Item(testItemId));
            }
        }

        /// <summary>
        ///A test for Create
        ///</summary>
        [TestMethod()]
        public override void Create_Test()
        {
            base.Create_Test();
        }

        /// <summary>
        ///A test for Enumerate
        ///</summary>
        [TestMethod()]
        public override void Enumerate_Test()
        {
            base.Enumerate_Test();
        }

        /// <summary>
        ///A test for GetEnumerator
        ///</summary>
        [TestMethod()]
        public override void GetEnumerator_Test()
        {
            base.GetEnumerator_Test();
        }

        /// <summary>
        ///A test for GetQuery
        ///</summary>
        [TestMethod()]
        public override void GetQuery_Test()
        {
            base.GetQuery_Test();
        }

        /// <summary>
        ///A test for Item
        ///</summary>
        [TestMethod()]
        public void Item_Test()
        {
            object key = new Guid("d2c6e9b3-3624-4d00-9285-d9072aa6ac3d");
            var actual = mRepository.Item(key);
            Assert.IsTrue(actual != null & actual.IsKeyEquals(key));
        }

        /// <summary>
        ///A test for Remove
        ///</summary>
        //[TestMethod()]
        public void Remove_Test()
        {
            mRepository.Remove(testItem);
            Assert.IsTrue(mRepository.Item(testItemId) == null);
        }

        /// <summary>
        ///A test for Update
        ///</summary>
       // [TestMethod()]
        public void Update_Test()
        {
            testItem.ChangedStatus = eArticleStatus.Final;
            mRepository.Update(testItem);
            Assert.IsTrue(((PublishResponce)mRepository.Item(testItemId)).ChangedStatus == testItem.ChangedStatus);
        }

        Guid testItemId = new Guid("00000000000000000000000000000001");
        PublishResponce testItem = new PublishResponce()
        {
            Id = new Guid("00000000000000000000000000000001"),
            ArticleInfo = new KeyValuePair<Guid, string>(new Guid("5a948975-9451-4a55-8718-e2faee49c565"), "Article"),
            PubliserInfo = new KeyValuePair<Guid, string>(new Guid("4992e5f0-3682-4859-a2b4-018c373250b3"), "David and Catherine Birnie"),
            ChangedStatus = eArticleStatus.OnRedaction,
            Date = DateTime.Now
        };
    }
}
