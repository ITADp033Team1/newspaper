﻿using SoftServe.Modules.NewsPaperExtensions.Controllers.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SoftServe.Modules.NewsPaperExtensions.Models;
using System.Collections.Generic;
using System.Linq;
using Ocorola.Entity.Infrastructure;
using Ocorola.Module.Infrastructure;
using SoftServe.Modules.NewsPaperExtensions;

namespace SoftServe.Testing.NewsPaperExtensionsTesting.RepositoriesTesting
{
    /// <summary>
    ///This is a test class for ArticleRepository
    ///</summary>
    [TestClass()]
    public class ArticleRepositoryTest : AbstractrepositoryTestClass<Article>
    {        

        /// <summary>
        ///A test for Add
        ///</summary>
        [TestMethod()]        
        public void AddTest()
        {
            if (mRepository.Item(testArticleId) != null)
            {
                try
                {
                    mRepository.Add(testArticle);
                }
                catch (InvalidOperationException ex)
                {
                    Assert.IsNotNull(ex);
                }
            }
            else
            {
                mRepository.Add(testArticle);
                Assert.IsNotNull(mRepository.Item(testArticleId));
            }            
        }
        

        /// <summary>
        ///A test for Create
        ///</summary>
        [TestMethod()]
        public override void Create_Test()
        {
            base.Create_Test();
        }

        /// <summary>
        ///A test for Enumerate
        ///</summary>
        [TestMethod()]
        public override void Enumerate_Test()
        {
            base.Enumerate_Test();
        }

        /// <summary>
        ///A test for GetEnumerator
        ///</summary>
        [TestMethod()]
        public override void GetEnumerator_Test()
        {
            base.GetEnumerator_Test();
        }

        /// <summary>
        ///A test for GetQuery
        ///</summary>
        [TestMethod()]
        public override void GetQuery_Test()
        {
            base.GetQuery_Test();
        }

        /// <summary>
        ///A test for Item
        ///</summary>
       // [TestMethod()]
        public void Item_Test()
        {
            object key = new Guid("5a948975-9451-4a55-8718-e2faee49c565");
            var actual = mRepository.Item(key);
            Assert.IsTrue(actual != null & actual.IsKeyEquals(key));
        }

        /// <summary>
        ///A test for Remove
        ///</summary>
        //[TestMethod()]
        public void Remove_Test()
        {         
            mRepository.Remove(testArticle);
            Assert.IsTrue(mRepository.Item(testArticleId) == null);
        }

        /// <summary>
        ///A test for Update
        ///</summary>
        [TestMethod()]
        public void Update_Test()
        {                        
            testArticle.Title = "UpdatedTitle";
            mRepository.Update(testArticle);
            Assert.IsTrue(((Article)mRepository.Item(testArticleId)).Title == testArticle.Title);
        }

        Guid testArticleId = new Guid("00000000000000000000000000000001");
        Article testArticle = new Article()
        {
            Id = new Guid("00000000000000000000000000000001"),
            Title = "Title",
            Updated = DateTime.Now,
            PictureUrl = "pic",
            Moderator = new KeyValuePair<Guid, string>(Guid.NewGuid(), "Moderator moderovich"),
            Content = "Article blablalb",
            Author = new KeyValuePair<Guid, string>(Guid.NewGuid(), "Author"),
            ArticleStatus = eArticleStatus.Final,
            //GroupId = Guid.Empty
        };
    }
}
