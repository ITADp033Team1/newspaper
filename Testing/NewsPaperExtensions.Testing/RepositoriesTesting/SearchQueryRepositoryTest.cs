﻿using SoftServe.Modules.NewsPaperExtensions.Controllers.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SoftServe.Modules.NewsPaperExtensions.Models;
using System.Collections.Generic;
using System.Linq;
using Ocorola.Entity.Infrastructure;

namespace SoftServe.Testing.NewsPaperExtensionsTesting.RepositoriesTesting
{
    
    
    /// <summary>
    ///This is a test class for SearchQueryRepositoryTest and is intended
    ///to contain all SearchQueryRepositoryTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SearchQueryRepositoryTest : AbstractrepositoryTestClass<SearchQuery>
    {

        [TestInitialize]
        public void Init()
        {
            foreach (var item in testItems)
            {
                mRepository.Add(item);
            }
        }

        [TestCleanup]
        public void Clean()
        {
            foreach (var item in mRepository)
            {
                mRepository.Remove(item);
            }
        }

        /// <summary>
        ///A test for Add
        ///</summary>
        [TestMethod()]
        public void AddTest()
        {
            mRepository.Add(testItem);
            Assert.IsNotNull(mRepository.Item(testItem.Id));
        }

        /// <summary>
        ///A test for Create
        ///</summary>
        [TestMethod()]
        public override void CreateTest()
        {
            base.CreateTest();
        }

        /// <summary>
        ///A test for Enumerate
        ///</summary>
        [TestMethod()]
        public override void EnumerateTest()
        {
            base.EnumerateTest();
        }

        /// <summary>
        ///A test for GetEnumerator
        ///</summary>
        [TestMethod()]
        public override void GetEnumeratorTest()
        {
            base.GetEnumeratorTest();
        }

        /// <summary>
        ///A test for GetQuery
        ///</summary>
        [TestMethod()]
        public override void GetQueryTest()
        {
            base.GetQueryTest();
        }

        /// <summary>
        ///A test for Item
        ///</summary>
        [TestMethod()]
        public void ItemTest()
        {
            var actual = mRepository.Item(testItems[0].Id);
            Assert.IsTrue(actual != null & actual.IsKeyEquals(testItems[0].Id));
        }

        /// <summary>
        ///A test for Remove
        ///</summary>
        [TestMethod()]
        public void RemoveTest()
        {
            mRepository.Remove(testItems[1]);
            Assert.IsTrue(mRepository.Item(testItems[1].Id) == null);
        }

        /// <summary>
        ///A test for Update
        ///</summary>
        [TestMethod()]
        public void UpdateTest()
        {
            var itemToChange = (SearchQuery)mRepository.Item(testItems[2].Id);
            itemToChange.Query = "Some Strange Query";
            mRepository.Update(itemToChange);
            Assert.IsTrue(((SearchQuery)mRepository
                .Item(testItems[2].Id)).Query == itemToChange.Query);
        }

        #region Test Data
        private static SearchQuery testItem = new SearchQuery()
        {
            Id = Guid.NewGuid(),
            NumberOfQuerys = 3,
            Query = "Curiosity",
            Tags = "Mars Space NASA Aliens"
        };

        private static List<SearchQuery> testItems = new List<SearchQuery>()
        {
            new SearchQuery() 
                { 
                    Id = Guid.NewGuid(),
                    NumberOfQuerys = 7,
                    Query = "Kendo",
                    Tags = "UI Japan Samurai"
                },
                new SearchQuery() 
                { 
                    Id = Guid.NewGuid(),
                    NumberOfQuerys = 2,
                    Query = "Rally",
                    Tags = "Cars Racing"
                },
                new SearchQuery() 
                { 
                    Id = Guid.NewGuid(),
                    NumberOfQuerys = 2,
                    Query = "Palestine",
                    Tags = "War Instability Israel Terror"
                },
                new SearchQuery() 
                { 
                    Id = Guid.NewGuid(),
                    NumberOfQuerys = 3,
                    Query = "Microsoft",
                    Tags = "Companies Windows 8 IT"
                }
        };
        #endregion
    }
}
