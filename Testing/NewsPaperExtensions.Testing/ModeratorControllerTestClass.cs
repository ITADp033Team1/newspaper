﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SoftServe.Modules.NewsPaperExtensions.Models;
using System.Xml.Serialization;
using System.IO;
using SoftServe.Modules.NewsPaperExtensions.Controllers;
using SoftServe.Modules.NewsPaperExtensions.Contracts;
using SoftServe.Modules.NewsPaperExtensions;
using SoftServe.Modules.NewsPaperExtensions.Abstractions;
using System.Runtime.Serialization;

namespace SoftServe.Testing.NewsPaperExtensionsTesting
{

    /// <summary>
    /// Summary description for ModeratorControllerTestClass
    /// </summary>
    [TestClass]
    public class ModeratorControllerTestClass
    {
        const string pathTemplate = "../../Stubs/{0}TestData.xml";


        [TestMethod]
        public void ModeratorController_GetRequests()
        {
            var serializer = new DataContractSerializer(typeof(List<PublishRequest>));
            using (var stream = new FileStream(string.Format(pathTemplate, "PublishRequest"), FileMode.Open, FileAccess.Read))
            {
                var data = serializer.ReadObject(stream) as List<PublishRequest>;
                using (var controller = LogicContainer.Instance.GetService<IModeratorContract>())
                {
                    var assertData = new List<PublishRequest>(controller.GetRequests());
                    Assert.AreEqual(assertData.Count, data.Count);
                }
            }
        }

        /// <summary>
        /// Create a request for approving article, approve it
        /// </summary>
        [TestMethod]
        public void ModeratorController_ApproveArticle()
        {
            Article article = new Article()
                {
                    Id = Guid.Parse("4aedbd8d-7229-47b1-9480-17cac56ab559")
                };
            using (var controller = LogicContainer.Instance.GetService<IModeratorContract>())
            {
                controller.ApproveArticle(article.Id);
                using (var storage = LogicContainer.Instance.GetService<Ocorola.Module.Infrastructure.IRepository<Article>>())
                {
                    var data = storage.Item(article.Id) as Article;
                    Assert.IsTrue(data.ArticleStatus == eArticleStatus.Approved);
                }
            }
        }

        /// <summary>
        /// Create request for approving article, decline it???
        /// </summary>
        [TestMethod]
        public void ModeratorController_DeclineArticle()
        {
            Article article = new Article()
            {
                Id = Guid.Parse("4aedbd8d-7229-47b1-9480-17cac56ab559")
            };
            string comment = "test";
            using (var controller = LogicContainer.Instance.GetService<IModeratorContract>())
            {
                controller.DeclineArticle(article.Id, comment);
                using (var storage = LogicContainer.Instance.GetService<Ocorola.Module.Infrastructure.IRepository<Article>>())
                {
                    var data = storage.Item(article.Id) as Article;
                    Assert.IsTrue(data.ArticleStatus == eArticleStatus.Declined);
                }
            }
        }
    }
}
