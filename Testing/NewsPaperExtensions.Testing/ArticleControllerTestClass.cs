﻿using System;
using System.Text;
using System.IO;
using SoftServe.Modules.NewsPaperExtensions.Contracts;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml.Serialization;
using SoftServe.Modules.NewsPaperExtensions.Models;
using System.Linq;
using SoftServe.Modules.NewsPaperExtensions.Abstractions;
using SoftServe.Modules.NewsPaperExtensions.Helpers;
using SoftServe.Modules.NewsPaperExtensions;
using Ocorola.Module.Infrastructure;
using SoftServe.Modules.NewsPaperExtensions.Controllers.Repositories;

namespace SoftServe.Testing.NewsPaperExtensionsTesting
{
    /// <summary>
    /// Unit-tests for ArticleControllerTestClass
    /// </summary>
    [TestClass]
    public class ArticleControllerTestClass
    {
        const string pathTemplate = "TestCaseData/{0}TestData.xml";

        [TestMethod]
        public void ArticleController_NewArticle()
        {
            using (var controller = SoftServe.Modules.NewsPaperExtensions.LogicContainer.Instance.GetService<IArticleContract>())
            {
                var article = new Article()
                {
                    Id = Guid.Parse("{00000000-0000-0000-0000-000000000001}"),
                    Author = new KeyValuePair<Guid, string>(ConstantHelper.DefaultAuthorId, ConstantHelper.DefaultAuthor),
                    GroupId = ConstantHelper.DefaultGroupId,
                    ArticleStatus = SoftServe.Modules.NewsPaperExtensions.Models.eArticleStatus.New,
                    Title = "Test"
                };

                controller.NewArticle(article);

                using (var repo = SoftServe.Modules.NewsPaperExtensions.LogicContainer.Instance.GetService<IRepository<Article>>())
                {
                    var returnArticle = repo.Item(article.Id);
                    Assert.IsNotNull(returnArticle);
                }
            }
        }

        [TestMethod]
        public void ArticleController_Update()
        {

            using (var controller = SoftServe.Modules.NewsPaperExtensions.LogicContainer.Instance.GetService<IArticleContract>())
            {
                var expected = new Article()
                {
                    Id = Guid.Parse("{00000000-0000-0000-0000-000000000001}"),
                    Author = new KeyValuePair<Guid, string>(ConstantHelper.DefaultAuthorId, ConstantHelper.DefaultAuthor),
                    GroupId = ConstantHelper.DefaultGroupId,
                    ArticleStatus = SoftServe.Modules.NewsPaperExtensions.Models.eArticleStatus.New,
                    Title = "Test"
                };

                Article article = controller.View(Guid.Parse("{00000000-0000-0000-0000-000000000001}"));

                Assert.IsNotNull(article);


                expected.AssignTo(article);
                controller.Update(article);

                //Relaod object
                article = controller.View(expected.Id);
                Assert.IsNotNull(article);
            }
        }

        [TestMethod]
        public void ArticleController_GetArticleHistory()
        {
            Guid id = ConstantHelper.DefaultArticleId;
            using (var controller = LogicContainer.Instance.GetService<IArticleContract>())
            {
                var query = from item in controller.GetArticleHistory(id)
                            select item;

                Assert.IsTrue((query).ToList().Count > -1);
            }
        }

        /// <summary>
        /// Write the test propely : 1. create request, send request
        /// </summary>
        [TestMethod]
        public void ArticleController_SendRequestToDelete()
        {
            Guid articleId = ConstantHelper.DefaultArticleId;
            using (var controller = LogicContainer.Instance.GetService<IArticleContract>())
            {
                controller.SendRequestToDelete(articleId);
            }
        }

        [TestMethod]
        public void ArticleController_DeleteArticle()
        {
            Guid articleId = ConstantHelper.DefaultArticleId;
            if (Guid.Empty.CompareTo(articleId) != 0)
            {
                using (var controller = LogicContainer.Instance.GetService<IArticleContract>())
                {
                    controller.DeleteArticle(articleId);
                    var test = controller.View(articleId);
                    Assert.IsNull(test);
                }
            }

        }

        [TestMethod]
        public void ArticleController_AddCommentToObject()
        {
            Guid articleId = ConstantHelper.DefaultArticleId;

            using (var controller = SoftServe.Modules.NewsPaperExtensions.LogicContainer.Instance.GetService<IArticleContract>())
            {
                Article article = controller.View(Guid.Parse("{00000000-0000-0000-0000-000000000001}"));

                Assert.IsNotNull(article);

                var cmId = Guid.NewGuid();
                controller.AddCommentToObject(articleId, new Comment() { Id = cmId });

                using (var storage = LogicContainer.Instance.GetService<IRepository<Comment>>())
                {
                    var comment = storage.Item(cmId);
                    Assert.IsNotNull(comment);
                }
            }
        }

        [TestMethod]
        public void ArticleController_GetComments()
        {
            Guid articleId = ConstantHelper.DefaultArticleId;
            Guid ownerId = ConstantHelper.DefaultAuthorId;
            using (var controller = LogicContainer.Instance.GetService<IArticleContract>())
            {
                Assert.IsTrue(controller.GetComments(articleId, ownerId).Count() > -1);
            }

        }

        [TestMethod]
        public void ArticleController_SetArticleStatus()
        {
            Article article = new Article();
            eArticleStatus status = eArticleStatus.OnRedaction;

            using (var controller = LogicContainer.Instance.GetService<IArticleContract>())
            {
                controller.SetArticleStatus(article, status);

                Assert.IsTrue(article.ArticleStatus == status);
            }

        }

        [TestMethod]
        public void ArticleController_View()
        {
            Guid articleId = Guid.Parse("{00000000-0000-0000-0000-000000000001}");

            using (var controller = LogicContainer.Instance.GetService<IArticleContract>())
            {
                var article = controller.View(articleId);

                Assert.IsNotNull(article);
            }
        }
    }
}
