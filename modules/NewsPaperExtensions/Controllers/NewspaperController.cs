﻿// <copyright file="NewspaperController.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Bilokin Sviatoslav</author>
// <date>3/25/2013 1:18:14 PM</date>
// <summary>TODO: Update summary</summary>
namespace SoftServe.Modules.NewsPaperExtensions.Controllers
{
    using SoftServe.Modules.NewsPaperExtensions.Contracts;
    using System;
    using System.Collections.Generic;
    using System.Text;
    using SoftServe.Modules.NewsPaperExtensions.Models;
    using SoftServe.Modules.NewsPaperExtensions.Abstractions;
    using Ocorola.Module.Infrastructure.Models;
    /// <summary>
    /// TODO: Update summary
    /// </summary>
    public class NewspaperController : INewsPaperModuleContract
    {
        public void DeleteUserProfile(Guid id)
        {
            using (var controller = SoftServe.Modules.NewsPaperExtensions.LogicContainer.Instance.GetService<IAdminContract>())
            {
                controller.DeleteUserProfile(id);
            }
        }

        public void SetUserProfileState(Guid id, eUserAccountState newState)
        {
            using (var controller = SoftServe.Modules.NewsPaperExtensions.LogicContainer.Instance.GetService<IAdminContract>())
            {
                controller.SetUserProfileState(id, newState);
            }
        }

        public ProfileBase GetUserProfile(Guid userAccointId)
        {
            using (var controller = SoftServe.Modules.NewsPaperExtensions.LogicContainer.Instance.GetService<IAdminContract>())
            {
               return controller.GetUserProfile(userAccointId);
            }
        }

        public string ResetUserPassword(Guid userAccointId)
        {
            using (var controller = SoftServe.Modules.NewsPaperExtensions.LogicContainer.Instance.GetService<IAdminContract>())
            {
                return controller.ResetUserPassword(userAccointId);
            }
        }

        public IEnumerable<Abstractions.ProfileBase> GetUsers(Abstractions.EntityFilterBase<Models.UserProfile> filter)
        {
            using (var controller = SoftServe.Modules.NewsPaperExtensions.LogicContainer.Instance.GetService<IAdminContract>())
            {
                return controller.GetUsers(filter);
            }
        }

        public void ApproveModeratorAccount(string login)
        {
            using (var controller = SoftServe.Modules.NewsPaperExtensions.LogicContainer.Instance.GetService<IAdminContract>())
            {
                controller.ApproveModeratorAccount(login);
            }
        }

        public void DeclineModeratorAccount(string login)
        {
            using (var controller = SoftServe.Modules.NewsPaperExtensions.LogicContainer.Instance.GetService<IAdminContract>())
            {
                controller.DeclineModeratorAccount(login);
            }
        }

        #region IDisposable implementation
        /// <summary>
        /// Implementation of IDisposable
        /// </summary>
        public void Dispose()
        {
            dispose(true);
            GC.SuppressFinalize(this);
        }

        private void dispose(bool disposeManaged)
        {
            if (mDisposed)
                return;
            mDisposed = true;
        }
        #endregion

        public Article NewArticle(Article article)
        {
            using (var controller = SoftServe.Modules.NewsPaperExtensions.LogicContainer.Instance.GetService<IArticleContract>())
            {
               return controller.NewArticle(article);
            }
        }

        public void Update(Article article)
        {
            using (var controller = SoftServe.Modules.NewsPaperExtensions.LogicContainer.Instance.GetService<IArticleContract>())
            {
                controller.Update(article);
            }
        }

        public IEnumerable<DomainModelBase> GetArticleHistory(Guid articleId)
        {
            using (var controller = SoftServe.Modules.NewsPaperExtensions.LogicContainer.Instance.GetService<IArticleContract>())
            {
               return controller.GetArticleHistory(articleId);
            }
        }

        public void SendRequestToDelete(Guid articleId)
        {
            using (var controller = SoftServe.Modules.NewsPaperExtensions.LogicContainer.Instance.GetService<IArticleContract>())
            {
                controller.SendRequestToDelete(articleId);
            }
        }

        public void DeleteArticle(Guid articleId)
        {
            using (var controller = SoftServe.Modules.NewsPaperExtensions.LogicContainer.Instance.GetService<IArticleContract>())
            {
                controller.DeleteArticle(articleId);
            }
        }

        public void AddCommentToObject(Guid objectId, Comment comment)
        {
            using (var controller = SoftServe.Modules.NewsPaperExtensions.LogicContainer.Instance.GetService<IArticleContract>())
            {
                controller.AddCommentToObject(objectId, comment);
            }
        }

        public IEnumerable<Comment> GetComments(Guid articleId, Guid ownerId)
        {
            using (var controller = SoftServe.Modules.NewsPaperExtensions.LogicContainer.Instance.GetService<IArticleContract>())
            {
               return controller.GetComments(articleId, ownerId);
            }
        }

        public void SetArticleStatus(Article article, eArticleStatus status)
        {
            using (var controller = SoftServe.Modules.NewsPaperExtensions.LogicContainer.Instance.GetService<IArticleContract>())
            {
                controller.SetArticleStatus(article, status);
            }
        }

        public Article View(Guid articleId)
        {
            using (var controller = SoftServe.Modules.NewsPaperExtensions.LogicContainer.Instance.GetService<IArticleContract>())
            {
               return controller.View(articleId);
            }
        }

        public IEnumerable<Article> LookupArticles(EntityFilterBase<Article> filter = null)
        {
            using (var controller = SoftServe.Modules.NewsPaperExtensions.LogicContainer.Instance.GetService<IArticleContract>())
            {
                return controller.LookupArticles(filter);
            }
        }

        public IEnumerable<PublishRequest> GetRequests(EntityFilterBase<PublishRequest> filter)
        {
            using (var controller = SoftServe.Modules.NewsPaperExtensions.LogicContainer.Instance.GetService<IModeratorContract>())
            {
              return  controller.GetRequests(filter);
            }
        }

        public void ApproveArticle(Guid articleId)
        {
            using (var controller = SoftServe.Modules.NewsPaperExtensions.LogicContainer.Instance.GetService<IModeratorContract>())
            {
                controller.ApproveArticle(articleId);
            }
        }

        public void DeclineArticle(Guid articleId, string comment)
        {
            using (var controller = SoftServe.Modules.NewsPaperExtensions.LogicContainer.Instance.GetService<IModeratorContract>())
            {
                controller.DeclineArticle(articleId, comment);
            }
        }

        private bool mDisposed = false;


        public IEnumerable<Comment> GetComments(Guid articleId)
        {
            throw new NotImplementedException();
        }
    }
}
