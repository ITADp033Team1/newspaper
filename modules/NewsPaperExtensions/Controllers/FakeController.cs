﻿using Ocorola.Module.Infrastructure.Controllers;
using SoftServe.Modules.NewsPaperExtensions.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftServe.Modules.NewsPaperExtensions.Controllers
{
    internal class FakeController:DomainControllerBase,IFakeContract
    {
        public FakeController():base(null,null)
        {

        }

        public int ReturnControlelrStatus()
        {
            return 5;
        }

        protected override void Initialize()
        {
            
        }
    }
}
