﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ocorola.Module.Infrastructure;
using Ocorola.Module.Infrastructure.Controllers;
using Ocorola.Module.Infrastructure.Models;
using SoftServe.Modules.NewsPaperExtensions.Contracts;
using SoftServe.Modules.NewsPaperExtensions.Models;
using SoftServe.Modules.NewsPaperExtensions.Abstractions;
using SoftServe.Modules.NewsPaperExtensions.Helpers;

namespace SoftServe.Modules.NewsPaperExtensions.Controllers
{
    internal class ArticleController : IArticleContract
    {
        private class GeneralSorter : IComparer<DomainModelBase>
        {
            public int Compare(DomainModelBase x, DomainModelBase y)
            {
                return 0;
            }
        }

        public ArticleController()
        // : base(LogicContainer.Instance.GetService<IStorageController>(), LogicContainer.Instance)
        {
        }


        /// <summary>
        /// Adding new article
        /// </summary>
        /// <param name="authorId">user Id</param>
        /// <param name="groupId"></param>
        /// <param name="subject">title</param>
        /// <returns></returns>
        public Article NewArticle(Article article)
        {
            if (article == null)
                throw new ArgumentNullException();
            // Extrat profile data
            var profileIfo = default(KeyValuePair<Guid, string>);
            using (var profileStorage = LogicContainer.Instance.GetService<IRepository<UserProfile>>())
            {
                var profile = profileStorage.Item(article.Author.Key) as UserProfile;
                if (profile == null)
                    throw new KeyNotFoundException();

                profileIfo = new KeyValuePair<Guid, string>(profile.Id, profile.FIO);
            }

            // Crate new Article
            using (var storage = LogicContainer.Instance.GetService<IRepository<Article>>())
            {
                var newEntity = storage.Create();
                newEntity.GroupId = article.GroupId;
                newEntity.Title = article.Title;
                newEntity.Author = profileIfo;
                newEntity.ArticleStatus = eArticleStatus.New;
                newEntity.Updated = DateTime.Now;
                return newEntity;
            }
        }

        /// <summary>
        /// Update article
        /// </summary>
        /// <param name="article">article to update</param>
        public void Update(Article article)
        {
            if (article != null)
            {
                using (var storage = LogicContainer.Instance.GetService<IRepository<Article>>())
                {
                    storage.Update(article);
                }
            }

        }

        /// <summary>
        /// Get a history of article
        /// </summary>
        /// <param name="articleId">id of article</param>
        /// <returns></returns>
        public IEnumerable<DomainModelBase> GetArticleHistory(Guid articleId)
        {
            if (Guid.Empty.CompareTo(articleId) == 0)
                throw new ArgumentException("articleId");

            List<DomainModelBase> pubRequestResp = new List<DomainModelBase>();

            //Get pub request list 
            using (var storage = LogicContainer.Instance.GetService<IRepository<PublishRequest>>())
            {
                var queryRequest = from pubreqst in storage.GetQuery()
                                   where pubreqst.ArticleInfo.Key == articleId
                                   orderby pubreqst.Date descending
                                   select pubreqst;

                foreach (var item in queryRequest.ToList())
                    pubRequestResp.Add(item);

            }

            //Get pub responce list 
            using (var storage = LogicContainer.Instance.GetService<IRepository<PublishResponce>>())
            {
                var queryRequest = from pubresp in storage.GetQuery()
                                   where pubresp.ArticleInfo.Key == articleId
                                   orderby pubresp.Date descending
                                   select pubresp;

                foreach (var item in queryRequest.ToList())
                    pubRequestResp.Add(item);
            }

            /// general sort

            pubRequestResp.Sort(new GeneralSorter());

            return pubRequestResp;
        }

        /// <summary>
        /// Send request for deleting article
        /// </summary>
        /// <param name="articleId">id of article</param>
        public void SendRequestToDelete(Guid articleId)
        {
            if (Guid.Empty.CompareTo(articleId) == 0)
                throw new ArgumentException("articleId");
        }

        /// <summary>
        /// Delete the article
        /// </summary>
        /// <param name="articleId">id of article</param>
        public void DeleteArticle(Guid articleId)
        {
            if (Guid.Empty.CompareTo(articleId) == 0)
                throw new ArgumentException("articleId");

            using (var storage = LogicContainer.Instance.GetService<IRepository<Article>>())
            {
                storage.Remove(new Article() { Id = articleId });
            }
        }

        /// <summary>
        /// Comment an object
        /// </summary>
        /// <param name="comment">comment</param>
        public void AddCommentToObject(Guid objectid, Comment comment)
        {
            if (comment == null)
                throw new ArgumentException("comment");


            using (var storage = LogicContainer.Instance.GetService<IRepository<Comment>>())
            {
                storage.Add(comment);
            }
        }

        /// <summary>
        /// Get all comments of the article
        /// </summary>
        /// <param name="articleId">id of article</param>
        /// <param name="ownerId">id of the owner</param>
        /// <returns>all comments</returns>
        public IEnumerable<Comment> GetComments(Guid articleId, Guid ownerId)
        {
            if (Guid.Empty.CompareTo(ownerId) == 0)
                throw new ArgumentException("ownerId");

            if (Guid.Empty.CompareTo(articleId) == 0)
                throw new ArgumentException("articleId");

            List<Comment> commentList = null;
            using (var storage = LogicContainer.Instance.GetService<IRepository<Comment>>())
            {

                if (!Guid.Empty.Equals(ownerId))
                {
                    commentList = (from comment in storage.GetQuery()
                                   where comment.PublisherID == ownerId
                                   orderby comment.Date descending
                                   select comment).ToList();

                }

                if (!Guid.Empty.Equals(ownerId))
                {
                    commentList = (from comment in storage.GetQuery()
                                   where comment.ParentId == articleId
                                   orderby comment.Date descending
                                   select comment).ToList();

                }
            }
            if (commentList == null)
                return new Comment[0];

            return commentList.AsEnumerable();
        }

        public IEnumerable<Comment> GetComments(Guid articleId)
        {
            if (Guid.Empty.CompareTo(articleId) == 0)
                throw new ArgumentException("articleId");

             List<Comment> commentList = null;
             using (var storage = LogicContainer.Instance.GetService<IRepository<Comment>>())
             {

                 if (!Guid.Empty.Equals(articleId))
                 {
                     commentList = (from comment in storage.GetQuery()
                                    where comment.ArticleId == articleId
                                    orderby comment.Date descending
                                    select comment).ToList();

                 }
             }

             if (commentList == null)
                 return new Comment[0];

             return commentList.AsEnumerable();
        }

        /// <summary>
        /// Set a status of article
        /// </summary>
        /// <param name="article">an article</param>
        /// <param name="status">new status</param>
        public void SetArticleStatus(Article article, eArticleStatus status)
        {
            if (article == null)
                throw new ArgumentNullException("article");

            if (article.ArticleStatus == Models.eArticleStatus.Final)
                throw new InvalidOperationException("You can't set new status to final article");

            using (var storage = LogicContainer.Instance.GetService<IRepository<Article>>())
            {
                storage.Update(article);
                article.ArticleStatus = status;
            }
        }

        /// <summary>
        /// View an article
        /// </summary>
        /// <param name="articleId">article id</param>
        /// <returns>article</returns>
        public Article View(Guid articleId)
        {
            using (var storage = LogicContainer.Instance.GetService<IRepository<Article>>())
            {
                return storage.Item(articleId) as Article;
            }
        }

        /// <summary>
        /// To watch articles
        /// </summary>
        /// <param name="filter">filter for articles</param>
        /// <returns>all articles appropriate for the filter</returns>
        public IEnumerable<Article> LookupArticles(EntityFilterBase<Article> filter = null)
        {
            if (filter == null)
            {
                using (var storage = LogicContainer.Instance.GetService<IRepository<Article>>())
                {
                    return storage.Enumerate();
                }
            }
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            dispose(true);
            GC.SuppressFinalize(this);
        }

        private void dispose(bool disposeManaged)
        {
            if (mDisposed)
                return;

            mDisposed = true;
        }

        private bool mDisposed = false;
    }
}

    

