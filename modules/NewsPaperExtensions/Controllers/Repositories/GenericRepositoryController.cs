﻿// -----------------------------------------------------------------------
// <copyright file="ArticleRepository.cs" company="Soft Serve Academy">
// This code was written by Evgeniy Galenko
// </copyright>
// -----------------------------------------------------------------------
namespace SoftServe.Modules.NewsPaperExtensions.Controllers.Repositories
{
    using Ocorola.Entity.Infrastructure;
    using Ocorola.Module.Infrastructure;
    using Ocorola.Module.Infrastructure.Controllers;
    using SoftServe.Modules.NewsPaperExtensions.Abstractions;
    using SoftServe.Modules.NewsPaperExtensions.Models;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization;
    using Ocorola.Module.Infrastructure.Models;

    /// <summary>
    /// Represents data repository for UserProfile model
    /// </summary>
    internal class GenericRepositoryController<DomainType> : ControllerBase, IRepository<DomainType>
        where DomainType: DomainModelBase, new()
    {
        /// <summary>
        /// Adds an item to the storage
        /// </summary>
        /// <param name="item">The object to add to the Storage</param>
        /// <exception cref="System.NotSupportedException">The storage does not support specified type</exception>
        /// <exception cref="System.ArgumentNullException">The added object is null</exception>
        /// <exception cref="System.InvalidOperationException">The storage already contains entity with key</exception>
        public void Add(DomainType item)
        {
            if (item == null)
                throw new ArgumentNullException("Adding object is null");

            if (m_DataStorage.Count<DomainType>(x => x.IsKeyEquals(item)) != 0)
                throw new InvalidOperationException("The storage already contains entity with such key");

            m_DataStorage.Add(item);
            Write();
        }

        /// <summary>
        /// Creates new instance of Entity's type
        /// </summary>        
        /// <returns>Instance of entity is suppoted type other vise null</returns>
        /// <exception cref="System.NotSupportedException">The storage does not support specified type</exception>
        public DomainType Create()
        {
            return new DomainType();
        }

        /// <summary>
        /// Enumerates everything entity is in storage is supported type
        /// </summary>
        /// <returns>Whole entities' set</returns>
        public IEnumerable<DomainType> Enumerate()
        {
            return m_DataStorage;
        }

        /// <summary>
        /// Enumerates everything entities are placed into storage by optional conditionals
        /// </summary>
        /// <returns></returns>
        public IQueryable<DomainType> GetQuery()
        {
            return m_DataStorage.AsQueryable<DomainType>();
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Gets entity by the key
        /// </summary>
        /// <param name="key">Key</param>        
        /// <exception cref="System.ArgumentNullException">The key is null</exception>
        /// <returns>Instance of entity or Nullable entity if found nothing</returns>
        public IStorageEntity Item(object key)
        {
            if (key == null)
                throw new ArgumentNullException("The key is null");

            return m_DataStorage.FirstOrDefault<DomainType>(item => item.IsKeyEquals(key));
        }

        /// <summary>
        /// Removes entity by entity's id You could pass no exactly obtained entity.
        /// Is is enough define keys of entity that should be removed
        /// </summary>
        /// <param name="entity">Entity to remove</param>
        /// <remarks>
        /// You could pass no exactly obtained entity. Is is enough define keys of entity
        /// that should be removed
        /// </remarks>
        public void Remove(DomainType entity)
        {
            m_DataStorage.Remove(m_DataStorage.Find(item => item.IsKeyEquals(entity)));
            Write();
        }

        /// <summary>
        /// Updates entity
        /// </summary>
        /// <param name="storageEntity">Entity</param>
        /// <exception cref="System.ArgumentNullException">The entity is null</exception>
        /// <exception cref="System.InvalidOperationException">The Entity's type was not initialized</exception>
        public void Update(DomainType storageEntity)
        {
            if (storageEntity == null)
                throw new ArgumentNullException("The entity is null");
            Remove(storageEntity);
            Add(storageEntity);
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns></returns>
        public IEnumerator<DomainType> GetEnumerator()
        {
            return m_DataStorage.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns></returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return m_DataStorage.GetEnumerator();
        }

        protected override void Dispose(bool disposeManaged)
        {
            if (m_Disposed)
                return;
            if (disposeManaged)
            {
            }
            m_Disposed = true;
        }

        protected override void Initialize()
        {
            if (File.Exists(m_StubDir))
            {
                DataContractSerializer serializer = new DataContractSerializer(typeof(List<DomainType>));
                using (var stream = new FileStream(m_StubDir, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    var tmp = (List<DomainType>)serializer.ReadObject(stream);
                    for (int i = 0; i < tmp.Count; i++)
                    {
                        m_DataStorage.Add((DomainType)tmp[i]);
                    }
                }
            }
        }     

        private bool m_Disposed = false;

        #region Data storage stub
        private List<DomainType> m_DataStorage = new List<DomainType>();

        private string m_StubDir = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/ArticleTestData.xml");


        private void Write()
        {
            DataContractSerializer serializer = new DataContractSerializer(typeof(List<DomainType>));
            File.Delete(m_StubDir);
            using (var stream = new FileStream(m_StubDir, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
            {
                List<DomainType> tmp = new List<DomainType>();
                for (int i = 0; i < m_DataStorage.Count; i++)
                {
                    tmp.Add(m_DataStorage[i]);
                }
                serializer.WriteObject(stream, tmp);
            }
        }
        #endregion

    }
}
