﻿// <copyright file="AdminController.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Kucher Inna</author>

namespace SoftServe.Modules.NewsPaperExtensions.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Linq;
    using Ocorola.Module.Infrastructure;
    using SoftServe.Modules.NewsPaperExtensions.Contracts;
    using SoftServe.Modules.NewsPaperExtensions.Models;
    using SoftServe.Modules.NewsPaperExtensions.Abstractions;
    using Ocorola.Module.Infrastructure.Controllers;

    /// <summary>
    /// Controller for admin's actions
    /// </summary>
    internal class AdminController : IAdminContract
    {
        public AdminController()
        {
            mUserRepository = LogicContainer.Instance.GetService<IRepository<UserProfile>>();
        }

        /// <summary>
        /// Delete user's profile
        /// </summary>
        /// <param name="id">users' id</param>
        public void DeleteUserProfile(Guid id)
        {
            if (Guid.Empty.CompareTo(id) == 0)
                throw new ArgumentException("id");
            var user = mUserRepository.Item(id);
            if (user != default(Ocorola.Entity.Infrastructure.IStorageEntity))
            {
                mUserRepository.Remove(new UserProfile { Id = id });
            }
        }

        /// <summary>
        /// Set state for a user
        /// </summary>
        /// <param name="id">user's id</param>
        /// <param name="newState">state</param>
        public void SetUserProfileState(Guid id, eUserAccountState newState)
        {
            if (Guid.Empty.CompareTo(id) == 0)
                throw new ArgumentException("id");
            var obj = mUserRepository.Item(id);
            if (obj != null)
            {
                var user = obj as UserProfile;
                user.UserStatus = newState;
                mUserRepository.Update(user);
            }

        }

        /// <summary>
        /// Watch a user's profile
        /// </summary>
        /// <param name="userAccountId">user's id</param>
        /// <returns>instance of profile</returns>
        public ProfileBase GetUserProfile(Guid userAccountId)
        {
            if (Guid.Empty.CompareTo(userAccountId) == 0)
                throw new ArgumentException("userAccountId");
            var user = mUserRepository.Item(userAccountId);
            if (user != default(Ocorola.Entity.Infrastructure.IStorageEntity))
            {
                return user as ProfileBase;
            }
            return default(UserProfile);
        }

        /// <summary>
        /// Reset a password of a user
        /// </summary>
        /// <param name="userAccountId">user's id</param>
        public string ResetUserPassword(Guid userAccountId)
        {
            if (Guid.Empty.CompareTo(userAccountId) == 0)
                throw new ArgumentException("userAccountId");
            var obj = mUserRepository.Item(userAccountId);
            if (obj != default(Ocorola.Entity.Infrastructure.IStorageEntity))
            {
                var user = obj as UserProfile;
                var random = new Random();
                var bytes = new byte[6];
                random.NextBytes(bytes);
                var generated = Encoding.UTF8.GetString(bytes);
                user.Password = generated;
                mUserRepository.Update(user);
                return generated;
            }
            return null;
        }

        /// <summary>
        /// Watch some users 
        /// </summary>
        /// <param name="filter">filter to choose users</param>
        /// <returns>collection of users</returns>
        public IEnumerable<ProfileBase> GetUsers(EntityFilterBase<UserProfile> filter = null)
        {
            using (var storage = LogicContainer.Instance.GetService<IRepository<UserProfile>>())
            {
                if (filter != null)
                {
                    return storage.GetQuery().Where(filter.Query.Expression as System.Linq.Expressions.Expression<Func<UserProfile, bool>>);
                }
                else
                {
                    return storage.ToList();
                }
            }

        }

        /// <summary>
        /// Approve user to become a moderator
        /// </summary>
        /// <param name="login">moderator login</param>
        public void ApproveModeratorAccount(string login)
        {
            if (string.IsNullOrEmpty(login))
                throw new ArgumentException("Login");

            var objs = mUserRepository.GetQuery().Where(u => u.Login == login);
            if (objs.Count() == 0)
            {
                return;
            }
            var user = objs.FirstOrDefault();
            using (var controller = LogicContainer.Instance.GetService<IRepository<ModeratorProfile>>())
            {
                var moderator = Helpers.MappingHelper.Map<UserProfile, ModeratorProfile>(user);
                if (!controller.Contains(moderator))
                {
                    controller.Add(moderator);
                    mUserRepository.Remove(user);
                }
            }


        }

        /// <summary>
        /// Decline user to become moderator
        /// </summary>
        /// <param name="login">user login</param>
        public void DeclineModeratorAccount(string login)
        {
            if (string.IsNullOrEmpty(login))
                throw new ArgumentException("Login");
            using (var controller = LogicContainer.Instance.GetService<IRepository<ModeratorProfile>>())
            {

                var objs = controller.GetQuery().Where(u => u.Login == login);
                if (objs == null)
                {
                    return;
                }
                var moderator = objs.FirstOrDefault();
                var user = Helpers.MappingHelper.Map<ModeratorProfile, UserProfile>(moderator);
                if (!mUserRepository.Contains(user))
                {
                    mUserRepository.Add(user);
                    controller.Remove(moderator);
                }
            }
        }

        #region IDisposable implementation
        /// <summary>
        /// Implementation of IDisposable
        /// </summary>
        public void Dispose()
        {
            dispose(true);
            GC.SuppressFinalize(this);
        }

        private void dispose(bool disposeManaged)
        {
            if (mDisposed)
                return;
            mUserRepository.Dispose();
            mDisposed = true;
        }
        #endregion

        private readonly IRepository<UserProfile> mUserRepository = default(IRepository<UserProfile>);
        private bool mDisposed = false;
    }
}
