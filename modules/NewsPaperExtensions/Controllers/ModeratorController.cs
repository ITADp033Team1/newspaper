﻿// <copyright file="ModeratorController.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Kucher Inna</author>

namespace SoftServe.Modules.NewsPaperExtensions.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using SoftServe.Modules.NewsPaperExtensions.Contracts;
    using SoftServe.Modules.NewsPaperExtensions.Models;
    using SoftServe.Modules.NewsPaperExtensions.Abstractions;
    using Ocorola.Module.Infrastructure;
    using System.Linq;

    /// <summary>
    /// Controller for moderator actions
    /// </summary>
    internal class ModeratorController : IModeratorContract
    {
        /// <summary>
        /// Watch resuests
        /// </summary>
        /// <param name="filter">filter for requests</param>
        /// <returns>collection of requests</returns>
        /// <exception cref=" System.ArgumentException">Arguments null or empty</exception>
        public IEnumerable<PublishRequest> GetRequests(EntityFilterBase<PublishRequest> filter = null)
        {
            using (var storage = LogicContainer.Instance.GetService<IRepository<PublishRequest>>())
            {
                if (filter != null)
                {
                    return storage.GetQuery().Where(filter.Query.Expression as System.Linq.Expressions.Expression<Func<PublishRequest, bool>>);
                }
                else
                {
                    return storage.ToList();
                }
            }

        }

        /// <summary>
        /// Approve article to be published
        /// </summary>
        /// <param name="articleId">id of article</param>
        ///exception cref=" System.ArgumentException">Arguments null or empty</exception>
        public void ApproveArticle(Guid articleId)
        {
            if (!Guid.Empty.Equals(articleId))
            {
                using (var storage = LogicContainer.Instance.GetService<IRepository<Article>>())
                {
                    var thing = storage.Item(articleId);
                    var article = thing as Article;
                    article.ArticleStatus = eArticleStatus.Approved;
                    article.Approved = DateTime.Now;
                    storage.Update(article);
                }
            }
        }

        /// <summary>
        /// Decline an article
        /// </summary>
        /// <param name="articleId">id of article</param>
        /// <param name="comment">Comment to article. Required</param>
        /// <exception cref=" System.ArgumentException">Arguments null or empty</exception>
        public void DeclineArticle(Guid articleId, string comment)
        {
            if (Guid.Empty.Equals(articleId))
                throw new ArgumentException("articleId");

            if (String.IsNullOrEmpty(comment))
                throw new ArgumentException("Comment should be written");

            using (var storage = LogicContainer.Instance.GetService<IRepository<Article>>())
            {
                var thing = storage.Item(articleId);
                var article = thing as Article;
                article.ArticleStatus = eArticleStatus.Declined;
                storage.Update(article);
            }
        }


        /// <summary>
        /// Implementation of IDispose
        /// </summary>
        public void Dispose()
        {
            dispose(true);
            GC.SuppressFinalize(this);
        }

        private void dispose(bool disposeManaged)
        {
            if (mDisposed)
                return;

            mDisposed = true;
        }

        private bool mDisposed = false;
    }
}
