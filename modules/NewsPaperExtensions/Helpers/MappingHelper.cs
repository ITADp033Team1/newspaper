﻿// <copyright file="MappingHelper.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Bilokin Sviatoslav</author>
// <date>3/25/2013 10:54:36 PM</date>
// <summary>TODO: Update summary</summary>
namespace SoftServe.Modules.NewsPaperExtensions.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    /// <summary>
    /// TODO: Update summary
    /// </summary>
    internal static class MappingHelper
    {
        public static TDestination Map<TInstance, TDestination>(TInstance instance) where TDestination : class
        {
            if (instance != null)
            {
                AutoMapper.Mapper.CreateMap<TInstance, TDestination>();
                return AutoMapper.Mapper.Map<TDestination>(instance) as TDestination;
            }
            return null;
        }
    }
}
