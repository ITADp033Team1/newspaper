﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftServe.Modules.NewsPaperExtensions.Helpers
{
    public static class ConstantHelper
    {
        public static readonly string DefaultAuthor = "John Smith";
        public static readonly Guid DefaultAuthorId = new Guid("{B86880D0-3FFA-4A65-868F-A46C025BC713}");
        public static readonly Guid DefaultGroupId = new Guid("{37C1EDFD-E1FB-4DB7-8A41-5CF811EDDFC1}");
        public static readonly Guid DefaultArticleId = new Guid("{B188104A-FFF0-4495-9CD5-F82E3C5901C5}");

    }
}
