﻿// <copyright file="IAdminContact.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Kucher Inna</author>

namespace SoftServe.Modules.NewsPaperExtensions.Contracts
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SoftServe.Modules.NewsPaperExtensions.Models;
    using SoftServe.Modules.NewsPaperExtensions.Abstractions;

    /// <summary>
    /// TODO: Update summary
    /// </summary>
    /// 
    public interface IAdminContract : IDisposable
    {
        /// <summary>
        /// Deletes user account from system
        /// </summary>
        /// <param name="id">account id</param>
        void DeleteUserProfile(Guid id);

        /// <summary>
        /// Changes state of existed account
        /// </summary>
        /// <param name="id">account id</param>
        /// <param name="newState"></param>
        void SetUserProfileState(Guid id, eUserAccountState newState);


        /// <summary>
        /// Recieves user account by id
        /// </summary>
        /// <param name="userAccointId"></param>
        /// <returns></returns>
        ProfileBase GetUserProfile(Guid userAccointId);

        /// <summary>
        /// Resets user password. only if account was blocked
        /// </summary>
        /// <param name="userAccointId"></param>
        string ResetUserPassword(Guid userAccointId);


        IEnumerable<ProfileBase> GetUsers(EntityFilterBase<UserProfile> filter = null);

        /// <summary>
        /// Grands moderator permissions to account
        /// </summary>
        /// <param name="login"></param>
        void ApproveModeratorAccount(string login);

        /// <summary>
        /// Revokes or decline moderator permissions from existed user user
        /// </summary>
        /// <param name="login"></param>
        void DeclineModeratorAccount(string login);
    }
}
