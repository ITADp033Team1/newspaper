﻿// <copyright file="IArticleContact.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Kucher Inna</author>

namespace SoftServe.Modules.NewsPaperExtensions.Contracts
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using SoftServe.Modules.NewsPaperExtensions.Models;
    using Ocorola.Module.Infrastructure.Models;
    using SoftServe.Modules.NewsPaperExtensions.Abstractions;

    /// <summary>
    /// TODO: Update summary
    /// </summary>
    /// 
    public interface IArticleContract : IDisposable 
    {        
        /// <summary>
        /// Creates new article according system defailt settings and subject
        /// </summary>
        /// <param name="authorId">published id, should have in publisher role</param>
        /// <param name="groupId">id of existed article group</param>
        /// <param name="subject">article subject</param>
        /// <returns>instanarce of created article or null if creation failed</returns>
        Article NewArticle(Article article);

        /// <summary>
        /// Updates existed article
        /// </summary>
        /// <param name="article">article object</param>
        void Update(Article article);

        /// <summary>
        /// Recieves full article history
        /// </summary>
        /// <param name="articleId">Article id</param>
        /// <returns>Collection of request & responces ordered from last to first by date</returns>
        IEnumerable<DomainModelBase> GetArticleHistory(Guid articleId);


        /// <summary>
        /// Provides service to remove own article what is already published
        /// </summary>
        /// <param name="articleId">Article id</param>
        void SendRequestToDelete(Guid articleId);


        /// <summary>
        /// Deletes not published article
        /// </summary>
        /// <param name="articleId">article id</param>
        void DeleteArticle(Guid articleId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objectId"></param>
        void AddCommentToObject(Guid objectId, Comment comment);

        IEnumerable<Comment> GetComments(Guid articleId);

        /// <summary>
        /// Recieves enumeration of commments for article.
        /// If ownerId eqals to article's publisher id then method should return only publisher comments
        /// </summary>
        /// <param name="articleId">Article's id</param>
        /// <param name="ownerId">ArticleOwner</param>
        /// <returns>List of comments or empty set</returns>
        IEnumerable<Comment> GetComments(Guid articleId, Guid ownerId);


        /// <summary>
        /// Setups status for existed article.
        /// If status of current article is Final, then status can't be chanhed
        /// </summary>
        /// <param name="article">arcticle id</param>
        /// <param name="status">new status</param>
        void SetArticleStatus(Article article, eArticleStatus status);
        
        
        /// <summary>
        /// Recieves article by id
        /// </summary>
        /// <param name="articleId"></param>
        /// <returns></returns>
        Article View(Guid articleId);

        /// <summary>
        /// Recieves all articles according filter data
        /// </summary>
        /// <param name="filter">filter data</param>
        /// <returns>Set of entities</returns>
        IEnumerable<Article> LookupArticles(EntityFilterBase<Article> filter = null);
    }
}
