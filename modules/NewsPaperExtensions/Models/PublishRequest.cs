﻿// <copyright file="PublishRequest.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Kucher Inna</author>

namespace SoftServe.Modules.NewsPaperExtensions.Models
{
    using System;
    using System.Collections.Generic;
    using System.Data.Services.Common;
    using System.Runtime.Serialization;
    using System.Text;
    using Ocorola.Module.Infrastructure.Models;
    using SoftServe.Modules.NewsPaperExtensions.Models;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Publish request for article.
    /// </summary>
    [DataContract]
    public class PublishRequest : DomainModelBase
    {
        #region Properties
        /// <summary>
        /// Request ID.
        /// </summary>
        [Key]
        [DataMember(Order = 1, Name = "id")]
        public Guid Id
        {
            get;
            set;
        }

        /// <summary>
        /// Article ID.
        /// </summary>
        [DataMember(Order = 2, Name = "articleInfo")]
        public KeyValuePair<Guid, string> ArticleInfo { get; set; }

        /// <summary>
        /// Publiser Id and Name of article with ArticleId
        /// </summary>
        [DataMember(Order = 4, Name = "publisher")]
        public KeyValuePair<Guid, string> PublisherInfo { get; set; }

        /// <summary>
        /// Date of creation.
        /// </summary>
        [DataMember(Order = 5, Name = "date")]
        public DateTime Date { get; set; }

        /// <summary>
        /// Header of request.
        /// </summary>
        [DataMember(Order = 6, IsRequired = true, Name = "subject")]
        public string Header { get; set; }

        /// <summary>
        /// Tags of article with ArticleID.
        /// </summary>
        [DataMember(Order = 5, IsRequired = true, Name = "tags")]
        public string Tags { get; set; }

        #endregion

        public override void AssignTo(object obj)
        {
            if (obj is PublishRequest)
            {
                var request = obj as PublishRequest;

                request.Id = Id;
                request.ArticleInfo = ArticleInfo;
                request.Date = Date;
                request.Header = Header;
                request.PublisherInfo = PublisherInfo;
                request.Tags = Tags;
                return;
            }
            throw new NotSupportedException();
        }

        public override bool IsKeyEquals( object key )
        {
            if (key is PublishRequest)
            {
                return Helpers.KeyHelper.IsGuidKeyEquals( key, (key as PublishRequest).Id );
            }
            return Helpers.KeyHelper.IsGuidKeyEquals( key, Id );
        }

        public bool Equals(PublishRequest other)
        {
            if (other == null)
                return false;
            if (!other.ArticleInfo.Key.Equals(ArticleInfo.Key))
                return false;
            if (string.Compare(other.ArticleInfo.Value, ArticleInfo.Value) != 0)
                return false;
            if (!other.Date.Equals(Date))
                return false;
            if (!other.Id.Equals(Id))
                return false;
            if (string.Compare(other.Header, Header) != 0)
                return false;
            if (!other.PublisherInfo.Key.Equals(PublisherInfo.Key))
                return false;
            if (string.Compare(other.PublisherInfo.Value, PublisherInfo.Value) != 0)
                return false;
            if (string.Compare(other.Tags, Tags) != 0)
                return false;

            return true;
        }
    }
}
