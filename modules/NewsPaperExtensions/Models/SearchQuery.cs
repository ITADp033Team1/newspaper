﻿// <copyright file="SearchQuery.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Bilokin Sviatoslav</author>
// <date>3/25/2013 1:39:55 PM</date>
// <summary>TODO: Update summary</summary>
namespace SoftServe.Modules.NewsPaperExtensions.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Runtime.Serialization;
    using System.Web;
    using Ocorola.Module.Infrastructure.Models;
    using System.ComponentModel.DataAnnotations;
    /// <summary>
    /// TODO: Update summary
    /// </summary>
    public class SearchQuery : DomainModelBase
    {
        static SearchQuery()
        {
            DomainModelBase.RegisterType<SearchQuery>();
        }
        #region Properties
        /// <summary>
        /// Id of object.
        /// </summary>
        [Key]
        [DataMember(Order = 1, IsRequired = true, Name = "SearchQueryId")]        
        public Guid Id { get; set; }
        /// <summary>
        /// Query itself.
        /// </summary>
        [DataMember(Order = 2, IsRequired = true, Name = "commentId")]
        public string Query { get; set; }
        /// <summary>
        /// Number of such query.
        /// </summary>
        [DataMember(Order = 3, IsRequired = true, Name = "numberOfQuerys")]
        public int NumberOfQuerys { get; set; }
        /// <summary>
        /// Matched tags for query.
        /// </summary>
        [DataMember(Order = 4, Name = "matchedTags")]
        public string Tags { get; set; }
        #endregion

        public override void AssignTo(object obj)
        {
            if (obj is SearchQuery)
            {
                var searchQuery = obj as SearchQuery;

                searchQuery.Id = Id;
                searchQuery.NumberOfQuerys = NumberOfQuerys;
                searchQuery.Query = Query;
                searchQuery.Tags = Tags;
                return;
            }
            throw new NotSupportedException();
        }

        public override bool IsKeyEquals(object key)
        {
            return Helpers.KeyHelper.IsGuidKeyEquals(key, Id);
        }

        public bool Equals(SearchQuery other)
        {
            if (other == null)
                return false;
            if (!other.Id.Equals(Id))
                return false;
            if (other.NumberOfQuerys != NumberOfQuerys)
                return false;
            if (string.Compare(other.Query, Query) != 0)
                return false;

            return true;
        }
    }
}
