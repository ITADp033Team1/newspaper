﻿// <copyright file="ArticleStatistics.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Kucher Inna</author>

namespace SoftServe.Modules.NewsPaperExtensions.Models
{
    using System;
    using System.Collections.Generic;
    using System.Data.Services.Common;
    using System.Runtime.Serialization;
    using System.Text;
    using Ocorola.Module.Infrastructure.Models;
    using SoftServe.Modules.NewsPaperExtensions.Models;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Article statistics.
    /// </summary>
    [DataContract, DataServiceEntity]
    public class ArticleStatistics : DomainModelBase
    {

        [DataMember(Order = 1, Name = "id of article", IsRequired = true)]
        public Guid ArticleId { get; set; }

        [DataMember(Order = 2, Name = "number of views")]
        public int NumberOfViews { get; set; }

        [DataMember(Order = 3, Name = "article status")]
        public eArticleStatus ArticleStatus { get; set; }

        [DataMember(Order = 4, Name = "words count")]
        public int WordsCount { get; set; }

        [DataMember(Order = 5, Name = "pages count")]
        public int PagesCount { get; set; }

        [DataMember(Order = 6, Name = "symbols count")]
        public int SymbolsCount { get; set; }

        public override void AssignTo(object obj)
        {
            if (obj is ArticleStatistics)
            {
                var articleStatistics = obj as ArticleStatistics;

                articleStatistics.NumberOfViews = NumberOfViews;
                articleStatistics.PagesCount = PagesCount;
                articleStatistics.SymbolsCount = SymbolsCount;
                articleStatistics.WordsCount = WordsCount;
                articleStatistics.ArticleStatus = ArticleStatus;
                
                return;
            }
            throw new NotSupportedException();
        }

        public bool Equals(ArticleStatistics other)
        {
            if (other == null)
                return false;

            if (!other.ArticleId.Equals(ArticleId))
                return false;
            if (other.ArticleStatus != ArticleStatus)
                return false;
            if (other.NumberOfViews != NumberOfViews)
                return false;
            if (other.PagesCount != PagesCount)
                return false;
            if (other.SymbolsCount != SymbolsCount)
                return false;
            if (other.WordsCount != WordsCount)
                return false;

            return true;
        }


        public override bool IsKeyEquals( object key )
        {
            if (key is ArticleStatistics)
            {
                return Helpers.KeyHelper.IsGuidKeyEquals( key, (key as ArticleStatistics).ArticleId );
            }
            return Helpers.KeyHelper.IsGuidKeyEquals( key, ArticleId );
        }
    }
}
