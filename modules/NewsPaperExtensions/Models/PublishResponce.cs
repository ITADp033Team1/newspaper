﻿// <copyright file="PublishResponce.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Kucher Inna</author>

namespace SoftServe.Modules.NewsPaperExtensions.Models
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using System.Text;
    using Ocorola.Module.Infrastructure.Models;
    using SoftServe.Modules.NewsPaperExtensions.Models;
    using System.ComponentModel.DataAnnotations;
    /// <summary>
    /// Publish responce for article.
    /// </summary>
    /// 
    [DataContract]
    public class PublishResponce : DomainModelBase
    {
        #region Properties
        
        /// <summary>
        /// Responce ID.
        /// </summary>
        [Key]
        [DataMember(Order = 1, Name = "id")]
        public Guid Id
        {
            get;
            set;
        }
        /// <summary>
        /// Article ID.
        /// </summary>
        [DataMember(Order = 2, Name = "article")]
        public KeyValuePair<Guid, string> ArticleInfo { get; set; }
        
        /// <summary>
        /// Publiser ID of article with ArticleID.
        /// </summary>
        [DataMember(Order = 3, IsRequired = true, Name = "publisher")]
        public KeyValuePair<Guid, string> PubliserInfo { get; set; }

        /// <summary>
        /// Status which should be applied to the article with ArticleID.
        /// </summary>
        [DataMember(Order = 4, Name = "status")]
        [EnumDataType(typeof(eArticleStatus))]
        public eArticleStatus ChangedStatus { get; set; }
        /// <summary>
        /// Date of responce' creation.
        /// </summary>
        [DataMember(Order = 5, Name = "date")]
        public DateTime Date { get; set; }
        #endregion

        public override void AssignTo(object obj)
        {
            if (obj is PublishResponce)
            {
                var responce = obj as PublishResponce;

                responce.Id = Id;
                responce.ArticleInfo = ArticleInfo;
                responce.ChangedStatus = ChangedStatus;
                responce.PubliserInfo = PubliserInfo;
                return;
            }
            throw new NotSupportedException();
        }

        public override bool IsKeyEquals(object key)
        {
            if(key is PublishResponce)
                return Helpers.KeyHelper.IsGuidKeyEquals( (key as PublishResponce).Id, Id );

            return Helpers.KeyHelper.IsGuidKeyEquals(key, Id);
        }

        public bool Equals(PublishResponce other)
        {
            if (other == null)
                return false;
            if (!other.ArticleInfo.Key.Equals(ArticleInfo.Key))
                return false;
            if (string.Compare(other.ArticleInfo.Value, ArticleInfo.Value) != 0)
                return false;
            if (other.ChangedStatus != ChangedStatus)
                return false;
            if (!other.Date.Equals(Date))
                return false;
            if (!other.Id.Equals(Id))
                return false;
            if (!other.PubliserInfo.Key.Equals(PubliserInfo.Key))
                return false;
            if (string.Compare(other.PubliserInfo.Value, PubliserInfo.Value) != 0)
                return false;

            return true;
        }
    }
}
