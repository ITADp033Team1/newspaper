﻿// <copyright file="eUserAccountState.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Kucher Inna</author>

namespace SoftServe.Modules.NewsPaperExtensions.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// TODO: Update summary
    /// </summary>
    /// 
    public enum eUserAccountState
    {

        /// <summary>
        /// User is not activated
        /// </summary>
        Inactive = 0,

        /// <summary>
        /// User is blocked by Admin
        /// </summary>
        Blocked = 1,

        /// <summary>
        /// Normal account state
        /// </summary>
        Active = 2
    }
}
