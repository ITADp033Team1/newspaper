﻿// <copyright file="ArticleHistory.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Kucher Inna</author>

namespace SoftServe.Modules.NewsPaperExtensions.Models
{
    using System;
    using System.Collections.Generic;
    using System.Data.Services.Common;
    using System.Runtime.Serialization;
    using System.Text;
    using Ocorola.Module.Infrastructure.Models;
    using SoftServe.Modules.NewsPaperExtensions.Models;

    /// <summary>
    /// TODO: Update summary
    /// </summary>
    /// 
    [DataContract, DataServiceEntity]
    public class ArticleHistory : DomainModelBase
    {
            [DataMember(Order = 1, Name = "id")]
            public Guid ArticleId { get; set; }

            [DataMember(Order = 2, Name = "items")]
            public IEnumerable<DomainModelBase> History { get; set; }

            public override void AssignTo(object obj)
            {
                if (obj is ArticleHistory)
                {
                    var articleHistory = obj as ArticleHistory;

                    articleHistory.ArticleId = ArticleId;
                    articleHistory.History = History;
                    return;
                }
                throw new NotSupportedException();
            }

            public override bool IsKeyEquals( object key )
            {
                if (key is ArticleHistory)
                {
                    return Helpers.KeyHelper.IsGuidKeyEquals( key, (key as ArticleHistory).ArticleId );
                }
                return Helpers.KeyHelper.IsGuidKeyEquals( key, ArticleId );
            }
        }
    }
