﻿// <copyright file="Class1.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Kucher Inna</author>

namespace SoftServe.Modules.NewsPaperExtensions.Abstractions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary
    /// </summary>
    public abstract class EntityFilterBase<Entity>
        where Entity : class
    {
        internal class DefaultEntityFilter : EntityFilterBase<Entity>
        {
            
        }
        public static EntityFilterBase<Entity> Default { get { return null; } }

        /// <summary>
        /// Number of page from Dataset
        /// </summary>
        public int PageNumber { get; set; }


        /// <summary>
        /// Size of Page in items
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// Query statement
        /// </summary>
        public IQueryable Query { get; set; }
    }
}
