﻿// <copyright file="IChangeStatus.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Kucher Inna</author>

namespace SoftServe.Modules.NewsPaperExtensions.Abstractions
{
    using SoftServe.Modules.NewsPaperExtensions.Models;
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// TODO: Update summary
    /// </summary>
    public interface IChangeStatus
    {
        eUserAccountState UserStatus { get; set; } 
    }
}
