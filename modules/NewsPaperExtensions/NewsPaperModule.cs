/// -----------------------------------------------------------------------
/// <copyright file="ExampleExtension.cs" company="">
/// TODO: Update copyright text.
/// </copyright>
/// -----------------------------------------------------------------------

namespace SoftServe.Modules.NewsPaperExtensions
{
    using System;
    using System.Collections.Generic;


    using Ocorola.Entity.Infrastructure;

    using Ocorola.Module.Infrastructure;
    using Ocorola.Module.Infrastructure.Models;
    using SoftServe.Modules.NewsPaperExtensions.Contracts;
    using SoftServe.Modules.NewsPaperExtensions.Abstractions;
    using SoftServe.Modules.NewsPaperExtensions.Models;

    /// <summary>
    /// TODO: add description
    /// </summary>
    internal class NewsPaperModule : DTEExtension, INewsPaperModuleContract
    {
        #region DTEExtension implementation

        public override ExtensionResponseBase Execute(ExtensionRequestBase command)
        {
            ///Don't implement this methid if you not plain implement communication with other similar modules
            throw new NotImplementedException();
        }

        public override ExtensionRequestBase CreateRequest(string requestId, string extensionId, Dictionary<string, object> parameters)
        {
            ///Don't implement this methid if you not plain implement communication with other similar modules
            throw new NotImplementedException();
        }

        public override void Dispose()
        {
            ///TODO: Implement module utilization logic here
            throw new NotImplementedException();
        }

        public override void OnConnection(object Application, Extensibility.ext_ConnectMode ConnectMode, object AddInInst, ref Array custom)
        {
            ///TODO: Implement module inilization logic here
            throw new NotImplementedException();
        }

        public override void OnDisconnection(Extensibility.ext_DisconnectMode RemoveMode, ref Array custom)
        {
            ///TODO: Implement module diconnection logic here
            throw new NotImplementedException();
        }

        #endregion

        #region INewsPaperModuleContract Members

        public void DeleteUserProfile(Guid id)
        {
            using (var controller = LogicContainer.Instance.GetService<IAdminContract>())
            {
                controller.DeleteUserProfile(id);
            }
        }

        public void SetUserProfileState(Guid id, Models.eUserAccountState newState)
        {
            using (var controller = LogicContainer.Instance.GetService<IAdminContract>())
            {
                controller.SetUserProfileState(id, newState);
            }
        }

        public Abstractions.ProfileBase GetUserProfile(Guid userAccointId)
        {
            using (var controller = LogicContainer.Instance.GetService<IAdminContract>())
            {
               return controller.GetUserProfile(userAccointId);
            }
        }

        public string ResetUserPassword(Guid userAccointId)
        {
            using (var controller = LogicContainer.Instance.GetService<IAdminContract>())
            {
                return controller.ResetUserPassword(userAccointId);
            }
        }

        public IEnumerable<Abstractions.ProfileBase> GetUsers(EntityFilterBase<Models.UserProfile> filter)
        {
            using (var controller = LogicContainer.Instance.GetService<IAdminContract>())
            {
                return controller.GetUsers(filter);
            }
        }

        public void ApproveModeratorAccount(string login)
        {
            using (var controller = LogicContainer.Instance.GetService<IAdminContract>())
            {
                controller.ApproveModeratorAccount(login);
            }
        }

        public void DeclineModeratorAccount(string login)
        {
            using (var controller = LogicContainer.Instance.GetService<IAdminContract>())
            {
                controller.DeclineModeratorAccount(login);
            }
        }

        public Models.Article NewArticle(Models.Article article)
        {
            using (var controller = LogicContainer.Instance.GetService<IArticleContract>())
            {
              return  controller.NewArticle(article);
            }
        }

        public void Update(Models.Article article)
        {
            using (var controller = LogicContainer.Instance.GetService<IArticleContract>())
            {
                controller.Update(article);
            }
        }

        public IEnumerable<DomainModelBase> GetArticleHistory(Guid articleId)
        {
            using (var controller = LogicContainer.Instance.GetService<IArticleContract>())
            {
                return controller.GetArticleHistory(articleId);
            }
        }

        public void SendRequestToDelete(Guid articleId)
        {
            using (var controller = LogicContainer.Instance.GetService<IArticleContract>())
            {
                controller.SendRequestToDelete(articleId);
            }
        }

        public void DeleteArticle(Guid articleId)
        {
            using (var controller = LogicContainer.Instance.GetService<IArticleContract>())
            {
                controller.DeleteArticle(articleId);
            }
        }

        public void AddCommentToObject(Guid objectId, Models.Comment comment)
        {
            using (var controller = LogicContainer.Instance.GetService<IArticleContract>())
            {
                controller.AddCommentToObject(objectId, comment);
            }
        }

        public IEnumerable<Models.Comment> GetComments(Guid articleId, Guid ownerId)
        {
            using (var controller = LogicContainer.Instance.GetService<IArticleContract>())
            {
                return controller.GetComments(articleId, ownerId);
            }
        }

        public void SetArticleStatus(Models.Article article, Models.eArticleStatus status)
        {
            using (var controller = LogicContainer.Instance.GetService<IArticleContract>())
            {
                controller.SetArticleStatus(article, status);
            }
        }

        public Models.Article View(Guid articleId)
        {
            using (var controller = LogicContainer.Instance.GetService<IArticleContract>())
            {
                return controller.View(articleId);
            }
        }

        public IEnumerable<Models.Article> LookupArticles(EntityFilterBase<Models.Article> filter)
        {
            using (var controller = LogicContainer.Instance.GetService<IArticleContract>())
            {
                return controller.LookupArticles(filter);
            }
        }

        public IEnumerable<Models.PublishRequest> GetRequests(EntityFilterBase<Models.PublishRequest> filter)
        {
            using (var controller = LogicContainer.Instance.GetService<IModeratorContract>())
            {
                return controller.GetRequests(filter);
            }
        }

        public void ApproveArticle(Guid articleId)
        {
            using (var controller = LogicContainer.Instance.GetService<IModeratorContract>())
            {
                controller.ApproveArticle(articleId);
            }
        }

        public void DeclineArticle(Guid articleId, string comment)
        {
            using (var controller = LogicContainer.Instance.GetService<IModeratorContract>())
            {
                controller.DeclineArticle(articleId, comment);
            }
        }



        public IEnumerable<Comment> GetComments(Guid articleId)
        {
            using (var controller = LogicContainer.Instance.GetService<IArticleContract>())
            {
              return  controller.GetComments(articleId);
            }
        }

        #endregion

    }
}