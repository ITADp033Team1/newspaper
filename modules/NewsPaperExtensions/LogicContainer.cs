﻿//<copyright file="LogicContainer.cs" company="SoftServe">
//Copyright (c) 2013 All Rights Reserved
//</copyright>
//<author>Sklyarskiy Alexander</author>
//<date>3/22/2013 1:05:22 AM</date>
//<summary>TODO: Update summary</summary>

using Microsoft.Practices.ObjectBuilder2;
using Microsoft.Practices.Unity;
using Ocorola.Module.Infrastructure;
using Ocorola.Module.Infrastructure.Abstractions;
using SoftServe.Modules.NewsPaperExtensions.Contracts;
using SoftServe.Modules.NewsPaperExtensions.Controllers;
using SoftServe.Modules.NewsPaperExtensions.Controllers.Repositories;
using SoftServe.Modules.NewsPaperExtensions.Models;
using System;
using System.Collections.Generic;

namespace SoftServe.Modules.NewsPaperExtensions
{
    public class LogicContainer : IServiceProviderExt
    {
        #region Nested converter class

        public class CustomParametersResolver : ResolverOverride
        {
            private readonly Queue<InjectionParameterValue> parameterValues;

            public CustomParametersResolver( IEnumerable<object> values )
            {
                parameterValues = new Queue<InjectionParameterValue>( );

                foreach (var parameterValue in values)
                    parameterValues.Enqueue( InjectionParameterValue.ToParameter( parameterValue ) );
            }

            public override IDependencyResolverPolicy GetResolver( IBuilderContext context, Type dependencyType )
            {
                if (parameterValues.Count < 1)
                    return null;

                var value = parameterValues.Dequeue( );
                return value.GetResolverPolicy( dependencyType );
            }
        }
        #endregion


        private LogicContainer()
        {
            m_Container = new UnityContainer( );
        }

        private void Initialize()
        {
            m_Container.RegisterType<IFakeContract, FakeController>( );
            m_Container.RegisterType(typeof(IRepository<>), typeof(GenericRepositoryController<>));
            #region old
            //m_Container.RegisterType<IRepository<Article>, AbstractRepositoryController<Article>>();
            //m_Container.RegisterType<IRepository<Comment>, CommentRepository>();
            //m_Container.RegisterType<IRepository<ModeratorProfile>, ModeratorProfileRepository>();
            //m_Container.RegisterType<IRepository<PublishRequest>, PublishRequestRepository>();
            //m_Container.RegisterType<IRepository<PublishResponce>, PublishResponseRepository>();            
            //m_Container.RegisterType<IRepository<PublisherProfile>, PublisherProfileRepository>();
            //m_Container.RegisterType<IRepository<ArticleHistory>, ArticleHistoryRepository>();
            //m_Container.RegisterType<IRepository<UserProfile>, UserProfileRepository>(); 
            #endregion
            m_Container.RegisterType<IAdminContract, AdminController>();
            m_Container.RegisterType<IModeratorContract, ModeratorController>();
            m_Container.RegisterType<INewsPaperModuleContract, NewspaperController>();
            m_Container.RegisterType<IArticleContract, ArticleController>();
        }


        #region Public Logic

        public object GetService( Type serviceType )
        {
            if(serviceType == typeof(IFakeContract))
                return m_Container.Resolve<IFakeContract>();

            if (serviceType == typeof(IStorageController))
                return m_Container.Resolve<IStorageController>();

            if (serviceType == typeof(IAdminContract))
                return m_Container.Resolve<IAdminContract>();

            if (serviceType == typeof(IArticleContract))
                return m_Container.Resolve<IArticleContract>();

            if (serviceType == typeof(IModeratorContract))
                return m_Container.Resolve<IModeratorContract>(); 

            if (serviceType == typeof(INewsPaperModuleContract))
                return m_Container.Resolve<INewsPaperModuleContract>();

            if (serviceType == typeof(IRepository<Article>))
                return m_Container.Resolve<IRepository<Article>>();

            if (serviceType == typeof(IRepository<Comment>))
                return m_Container.Resolve<IRepository<Comment>>();

            if (serviceType == typeof(IRepository<UserProfile>))
                return m_Container.Resolve<IRepository<UserProfile>>();

            if (serviceType == typeof(IRepository<ModeratorProfile>))
                return m_Container.Resolve<IRepository<ModeratorProfile>>();

            if (serviceType == typeof(IRepository<PublisherProfile>))
                return m_Container.Resolve<IRepository<PublisherProfile>>();

            if (serviceType == typeof(IRepository<PublishRequest>))
                return m_Container.Resolve<IRepository<PublishRequest>>(); 
            return null;
        }

        public T GetService<T>()
        {
            return m_Container.Resolve<T>( );
        }

        public T GetService<T>( object[] values )
        {
            if (values == null)
                return m_Container.Resolve<T>( );

            return m_Container.Resolve<T>( new CustomParametersResolver( values ) );
        }
        #endregion

        #region Singleton
        public static IServiceProviderExt Instance
        {
            get
            {
                lock (_mutex)
                {
                    if (m_Instance == null)
                    {
                        m_Instance = new LogicContainer( );
                        m_Instance.Initialize( );
                    }
                }
                return m_Instance;
            }
        }
        #endregion

        #region Fields

        private readonly IUnityContainer m_Container = default( IUnityContainer );

        /// Static fields
        private static readonly object _mutex = new object( );
        private static LogicContainer m_Instance = default( LogicContainer );

        #endregion
    }
}
