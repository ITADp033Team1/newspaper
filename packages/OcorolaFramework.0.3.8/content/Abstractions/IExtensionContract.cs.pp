﻿/// -----------------------------------------------------------------------
/// <copyright file="IExtensionContract.cs" company="">
/// TODO: Update copyright text.
/// </copyright>
/// -----------------------------------------------------------------------

namespace $rootnamespace$.Contracts
{
    using System;

    /// <summary>
    /// Example extension contract
    /// </summary>
    public interface IExtensionContract : IDisposable
    {
        /// <summary>
        /// Computes salary with rate & worked hours
        /// </summary>
        /// <param name="rate">Personal rate</param>
        /// <param name="hours">Worked hours</param>
        /// <exception cref="ArgumentException">When hours less than 0 or rete less than 0</exception>
        /// <returns>Computed salary</returns>
        decimal ComputeSalary(decimal rate, int hours);
    }
}
