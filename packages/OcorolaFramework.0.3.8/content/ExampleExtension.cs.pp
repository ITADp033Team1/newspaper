﻿/// -----------------------------------------------------------------------
/// <copyright file="ExampleExtension.cs" company="">
/// TODO: Update copyright text.
/// </copyright>
/// -----------------------------------------------------------------------

namespace $rootnamespace$
{
    using System;
    using System.Collections.Generic;


    using Ocorola.Entity.Infrastructure;

    using Ocorola.Module.Infrastructure;
    using Ocorola.Module.Infrastructure.Models;

    /// <summary>
    /// Example application module
    /// </summary>
    public class ExampleExtension: DTEExtension, Contracts.IExtensionContract
    {
        #region DTEExtension implementation

        public override ExtensionResponseBase Execute(ExtensionRequestBase command)
        {
            ///Don't implement this methid if you not plain implement communication with other similar modules
            throw new NotImplementedException();
        }

        public override ExtensionRequestBase CreateRequest(string requestId, string extensionId, Dictionary<string, object> parameters)
        {
            ///Don't implement this methid if you not plain implement communication with other similar modules
            throw new NotImplementedException();
        }

        public override void Dispose()
        {
            ///TODO: Implement module utilization logic here
            throw new NotImplementedException();
        }

        public override void OnConnection(object Application, Extensibility.ext_ConnectMode ConnectMode, object AddInInst, ref Array custom)
        {
            ///TODO: Implement module inilization logic here
            throw new NotImplementedException();
        }

        public override void OnDisconnection(Extensibility.ext_DisconnectMode RemoveMode, ref Array custom)
        {
            ///TODO: Implement module diconnection logic here
            throw new NotImplementedException();
        }

        #endregion
      
        #region IExtensionContract Members

        decimal Contracts.IExtensionContract.ComputeSalary(decimal rate, int hours)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
