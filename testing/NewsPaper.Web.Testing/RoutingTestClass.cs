﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using NewsPaperWeb;
using System.Reflection;

namespace SoftServe.Testing.NewsPaperWebTesting
{
    /// <summary>
    /// Class for routing testing
    /// </summary>
    [TestClass]
    public class RoutingTestClass
    {
        private System.Web.HttpContextBase CreateHttpContext(string targetURL = null, string httpMethod = "GET")
        {
            Mock<System.Web.HttpRequestBase> mockRequest = new Mock<System.Web.HttpRequestBase>();
            mockRequest.Setup(m => m.AppRelativeCurrentExecutionFilePath).Returns(targetURL);
            mockRequest.Setup(m => m.HttpMethod).Returns(httpMethod);

            Mock<System.Web.HttpResponseBase> mockResponse = new Mock<System.Web.HttpResponseBase>();
            mockResponse.Setup(m => m.ApplyAppPathModifier(It.IsAny<string>())).Returns<string>(s=>s);

            Mock<System.Web.HttpContextBase> mockContext = new Mock<System.Web.HttpContextBase>();
            mockContext.Setup(m => m.Request).Returns(mockRequest.Object);
            mockContext.Setup(m => m.Response).Returns(mockResponse.Object);

            return mockContext.Object;
        }

        private void TestRouteMatch(string url, string controller, string action, object routeProperties = null, string httpMethod = "GET")
        {
            RouteCollection routes = new RouteCollection();
            MvcApplication.RegisterRoutes(routes);

            var context = CreateHttpContext(url, httpMethod);
            var result = routes.GetRouteData(context);


            Assert.IsNotNull(result);
            Assert.IsTrue(TestIncomingResult(result,controller,action,routeProperties));
        }

        private bool TestIncomingResult(RouteData routeResult, string controller, string action, object PropertySet = null)
        {
            Func<object, object, bool> valCompare = (v1, v2) =>
            {
                return StringComparer.InvariantCultureIgnoreCase.Compare(v1, v2) == 0;
            };

            bool result = valCompare(routeResult.Values["controller"], controller) && valCompare(routeResult.Values["action"], action);
            if (PropertySet != null)
            {
                PropertyInfo[] pinfo = PropertySet.GetType().GetProperties();
                foreach (PropertyInfo item in pinfo)
                {
                    if (!(routeResult.Values.ContainsKey(item.Name) && valCompare(routeResult.Values[item.Name],item.GetValue(PropertySet,null))))
                    {
                        result = false;
                        break;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// To test incoming routes
        /// </summary>
        [TestMethod]
        public void TestIncomingRoutes()
        {
            TestRouteMatch("~/Home/Index", "Home", "Index");
            TestRouteMatch("~/Errors/Http404", "Errors", "Http404");
            TestRouteFail("~/ieurbgibeiurg/iojroijgioerg");     
        }

        /// <summary>
        /// Test which checks if test route was failing
        /// </summary>
        /// <param name="url">url to go</param>
        [TestMethod]
        private void TestRouteFail(string url)
        {
            RouteCollection routes = new RouteCollection();
            RouteData result = routes.GetRouteData(CreateHttpContext(url));
            Assert.IsTrue( result == null || result.Route == null);

        }


    }
}
