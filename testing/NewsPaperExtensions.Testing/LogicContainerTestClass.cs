﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SoftServe.Modules.NewsPaperExtensions;
using SoftServe.Modules.NewsPaperExtensions.Contracts;

namespace SoftServe.Testing.NewsPaperExtensionsTesting
{
    [TestClass]
    public class LogicContainerTestClass
    {
        [TestMethod]
        public void Test_LogicContainer_GetService_generic()
        {
            using (var controller = LogicContainer.Instance.GetService<IFakeContract>( ))
            {
                Assert.IsTrue( controller.ReturnControlelrStatus( ) == 5 );
            }
        }

        [TestMethod]
        public void Test_LogicContainer_GetService_genericWithParameters()
        {
            using (var controller = LogicContainer.Instance.GetService<IFakeContract>( new object[] { 5 } ))
            {
                Assert.IsTrue( controller.ReturnControlelrStatus( ) == 5 );
            }
        }

        [TestMethod]
        public void Test_LogicContainer_GetService_classic()
        {
            using (var controller = (IFakeContract)LogicContainer.Instance.GetService( typeof( IFakeContract ) ))
            {
                Assert.IsTrue( controller.ReturnControlelrStatus( ) == 5 );
            }
        }
    }
}
