﻿using SoftServe.Modules.NewsPaperExtensions.Controllers.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SoftServe.Modules.NewsPaperExtensions.Models;
using System.Collections.Generic;
using System.Linq;
using Ocorola.Entity.Infrastructure;
using SoftServe.Testing.NewsPaperExtensionsTesting.RepositoriesTesting;

namespace SoftServe.Testing.NewsPaperExtensionsTesting.RepositoriesTesting
{
    
    
    /// <summary>
    ///This is a test class for ArticleStatisticsRepositoryTest and is intended
    ///to contain all ArticleStatisticsRepositoryTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ArticleStatisticsRepositoryTest : AbstractrepositoryTestClass<ArticleStatistics>
    {

        [TestInitialize]
        public void Init()
        {
            foreach (var item in testItems)
            {
                mRepository.Add(item);
            }
        }

        [TestCleanup]
        public void Clean()
        {            
            foreach (var item in mRepository)
            {
                mRepository.Remove(item);
            }            
        }

        /// <summary>
        ///A test for Add
        ///</summary>
        [TestMethod()]
        public void AddTest()
        {
            mRepository.Add(testItem);
            Assert.IsNotNull(mRepository.Item(testItem.ArticleId));
        }

        /// <summary>
        ///A test for Create
        ///</summary>
        [TestMethod()]
        public override void CreateTest()
        {
            base.CreateTest();
        }

        /// <summary>
        ///A test for Enumerate
        ///</summary>
        [TestMethod()]
        public override void EnumerateTest()
        {
            base.EnumerateTest();
        }

        /// <summary>
        ///A test for GetEnumerator
        ///</summary>
        [TestMethod()]
        public override void GetEnumeratorTest()
        {
            base.GetEnumeratorTest();
        }

        /// <summary>
        ///A test for GetQuery
        ///</summary>
        [TestMethod()]
        public override void GetQueryTest()
        {                        
            base.GetQueryTest();
        }

        /// <summary>
        ///A test for Item
        ///</summary>
        [TestMethod()]
        public void ItemTest()
        {
            var actual = mRepository.Item(testItems[0].ArticleId);
            Assert.IsTrue(actual != null & actual.IsKeyEquals(testItems[0].ArticleId));
        }

        /// <summary>
        ///A test for Remove
        ///</summary>
        [TestMethod()]
        public void RemoveTest()
        {
            mRepository.Remove(testItems[1]);
            Assert.IsTrue(mRepository.Item(testItems[1].ArticleId) == null);
        }

        /// <summary>
        ///A test for Update
        ///</summary>
        [TestMethod()]
        public void UpdateTest()
        {
            var itemToChange = (ArticleStatistics)mRepository.Item(testItems[2].ArticleId);
            itemToChange.ArticleStatus = eArticleStatus.Approved;
            mRepository.Update(itemToChange);
            Assert.IsTrue(((ArticleStatistics)mRepository
                .Item(testItems[2].ArticleId)).ArticleStatus == itemToChange.ArticleStatus);
        }

        #region Test Data
        private static ArticleStatistics testItem = new ArticleStatistics()
        {
            ArticleId = Guid.NewGuid(),
            ArticleStatus = eArticleStatus.New,
            NumberOfViews = 12,
            PagesCount = 6,
            SymbolsCount = 24124321,
            WordsCount = 34532
        };

        private static List<ArticleStatistics> testItems = new List<ArticleStatistics>()
        {
            new ArticleStatistics()
            {
            ArticleId = Guid.NewGuid(),
            ArticleStatus = eArticleStatus.New,
            NumberOfViews = 54,
            PagesCount = 3,
            SymbolsCount = 345432,
            WordsCount = 5000
            },
            new ArticleStatistics()
            {
            ArticleId = Guid.NewGuid(),
            ArticleStatus = eArticleStatus.OnRedaction,
            NumberOfViews = 77,
            PagesCount = 1,
            SymbolsCount = 3242,
            WordsCount = 953
            },
            new ArticleStatistics()
            {
            ArticleId = Guid.NewGuid(),
            ArticleStatus = eArticleStatus.Declined,
            NumberOfViews = 66,
            PagesCount = 2,
            SymbolsCount = 8755,
            WordsCount = 542
            },
            new ArticleStatistics()
            {
            ArticleId = Guid.NewGuid(),
            ArticleStatus = eArticleStatus.Final,
            NumberOfViews = 23,
            PagesCount = 4,
            SymbolsCount = 9739,
            WordsCount = 3422
            }
        };
        #endregion
    }
}
