﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SoftServe.Modules.NewsPaperExtensions;
using Ocorola.Module.Infrastructure;
using SoftServe.Modules.NewsPaperExtensions.Models;
using System.IO;
using System.Runtime.Serialization;

namespace SoftServe.Testing.NewsPaperExtensionsTesting.RepositoriesTesting
{
    /// <summary>
    /// Summary description for GenericRepositoryTestClass
    /// </summary>
    [TestClass]
    public class GenericRepositoryTestClass
    {
        public GenericRepositoryTestClass()
        {
            mGuid = Guid.NewGuid();
        }


        #region TestContext
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        } 
        #endregion

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod, DeploymentItem("RepositoryTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
            "|DataDirectory|\\RepositoryTestData.xml",
            "Row", 
            Microsoft.VisualStudio.TestTools.UnitTesting.DataAccessMethod.Sequential)]
        public void Add_ActionTest()
        {
            #region Arrange
            var data = this.TestContext.DataRow["Type"];
            var logic = LogicContainer.Instance;
            var repotype = typeof(IRepository<>);
            var type = Type.GetType(string.Format("{1}.{0}, {2}", data.ToString(), "SoftServe.Modules.NewsPaperExtensions.Models", "NewsPaperExtensions"));
            var m = repotype.MakeGenericType(type);
            var repository = logic.GetService(m);
            var instance = type.GetConstructor(new Type[0]).Invoke(new object[0]);
            type.InvokeMember("Id",
    System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.SetProperty,
    Type.DefaultBinder, instance, new object[] { mGuid }); 
            #endregion

            #region Act
            var method = repository.GetType().GetMethod("Add", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.InvokeMethod);
            method.Invoke(repository, new object[] { instance }); 
            #endregion

            #region Assert
            var item =  repository.GetType().GetMethod("Item", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.InvokeMethod);
            var obj = item.Invoke(repository, new object[] { mGuid }); 
            Assert.IsNotNull(obj);
            #endregion
        }

        [TestMethod, DeploymentItem("RepositoryTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
            "|DataDirectory|\\RepositoryTestData.xml",
            "Row",
            Microsoft.VisualStudio.TestTools.UnitTesting.DataAccessMethod.Sequential)]
        public void Update_ActionTest()
        {
            #region Arrange
            var data = this.TestContext.DataRow["Type"];
            var logic = LogicContainer.Instance;
            var repotype = typeof(IRepository<>);
            var type = Type.GetType(string.Format("{1}.{0}, {2}", data.ToString(), "SoftServe.Modules.NewsPaperExtensions.Models", "NewsPaperExtensions"));
            var m = repotype.MakeGenericType(type);
            var repository = logic.GetService(m);
            var instance = type.GetConstructor(new Type[0]).Invoke(new object[0]);
            type.InvokeMember("Id",
    System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.SetProperty,
    Type.DefaultBinder, instance, new object[] { mGuid });
            #endregion

            #region Act
            var method = repository.GetType().GetMethod("Update", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.InvokeMethod);
            method.Invoke(repository, new object[] { instance });
            #endregion

            #region Assert
            var item = repository.GetType().GetMethod("Item", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.InvokeMethod);
            var obj = item.Invoke(repository, new object[] { mGuid });
            Assert.IsNotNull(obj);
            #endregion
        }

        [TestMethod, DeploymentItem("RepositoryTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
            "|DataDirectory|\\RepositoryTestData.xml",
            "Row",
            Microsoft.VisualStudio.TestTools.UnitTesting.DataAccessMethod.Sequential)]
        public void Remove_ActionTest()
        {
            #region Arrange
            var data = this.TestContext.DataRow["Type"];
            var logic = LogicContainer.Instance;
            var repotype = typeof(IRepository<>);
            var type = Type.GetType(string.Format("{1}.{0}, {2}", data.ToString(), "SoftServe.Modules.NewsPaperExtensions.Models", "NewsPaperExtensions"));
            var m = repotype.MakeGenericType(type);
            var repository = logic.GetService(m);
            var instance = type.GetConstructor(new Type[0]).Invoke(new object[0]);
            type.InvokeMember("Id",
    System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.SetProperty,
    Type.DefaultBinder, instance, new object[] { mGuid });
            #endregion

            #region Act
            var method = repository.GetType().GetMethod("Remove", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.InvokeMethod);
            method.Invoke(repository, new object[] { instance });
            #endregion

            #region Assert
            var item = repository.GetType().GetMethod("Item", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.InvokeMethod);
            var obj = item.Invoke(repository, new object[] { mGuid });
            Assert.IsNull(obj);
            #endregion
        }

        [TestMethod, DeploymentItem("RepositoryTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
    "|DataDirectory|\\RepositoryTestData.xml",
    "Row",
    Microsoft.VisualStudio.TestTools.UnitTesting.DataAccessMethod.Sequential)]
        public void GetEnumerator_ActionTest()
        {
            #region Arrange
            var data = this.TestContext.DataRow["Type"];
            var logic = LogicContainer.Instance;
            var repotype = typeof(IRepository<>);
            var type = Type.GetType(string.Format("{1}.{0}, {2}", data.ToString(), "SoftServe.Modules.NewsPaperExtensions.Models", "NewsPaperExtensions"));
            var m = repotype.MakeGenericType(type);
            var repository = logic.GetService(m);

            #endregion

            #region Act
            var item = repository.GetType().GetMethod("GetEnumerator", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.InvokeMethod);
            var obj = item.Invoke(repository, new object[] { });
            #endregion

            #region Assert
            Assert.IsNotNull(obj);
            #endregion
        }

        [TestMethod, DeploymentItem("RepositoryTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
    "|DataDirectory|\\RepositoryTestData.xml",
    "Row",
    Microsoft.VisualStudio.TestTools.UnitTesting.DataAccessMethod.Sequential)]
        public void GetQuery_ActionTest()
        {
            #region Arrange
            var data = this.TestContext.DataRow["Type"];
            var logic = LogicContainer.Instance;
            var repotype = typeof(IRepository<>);
            var type = Type.GetType(string.Format("{1}.{0}, {2}", data.ToString(), "SoftServe.Modules.NewsPaperExtensions.Models", "NewsPaperExtensions"));
            var m = repotype.MakeGenericType(type);
            var repository = logic.GetService(m);
            #endregion

            #region Act
            var item = repository.GetType().GetMethod("GetQuery", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.InvokeMethod);
            var obj = item.Invoke(repository, new object[] { });
            #endregion

            #region Assert
            Assert.IsNotNull(obj);
            #endregion
        }

        #region Fields
        private Guid mGuid = default(Guid);
        private TestContext testContextInstance; 
        #endregion
    }
}
