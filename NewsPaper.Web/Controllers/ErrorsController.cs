﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SoftServe.NewsPaperWeb.Controllers
{
    public class ErrorsController : Controller
    {
        //
        // GET: /Errors/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Errors/Details/5

        public ActionResult Http404()
        {
            return View();
        }

        public ActionResult Http401()
        {
            return View();
        }

        public ActionResult Http400()
        {
            return View();
        }

        public ActionResult Http500()
        {
            return View();
        }

        public ActionResult Http501()
        {
            return View();
        }
    }
}
