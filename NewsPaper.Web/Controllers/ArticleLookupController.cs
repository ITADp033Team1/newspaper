﻿using SoftServe.Modules.NewsPaperExtensions;
using SoftServe.Modules.NewsPaperExtensions.Contracts;
using SoftServe.Modules.NewsPaperExtensions.Models;
using SoftServe.NewsPaperWeb.Filters;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SoftServe.NewsPaperWeb.Controllers
{
    public class ArticleLookupController : ApiController
    {
        static ArticleLookupController()
        {
            mModuleContract = LogicContainer.Instance.GetService<INewsPaperModuleContract>();
        }

        public HttpResponseMessage GetArticlesListByFilter([FromBody]Filters.ArticleFilter filter)
        {
            if (filter == null)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
            var data = mModuleContract.LookupArticles(filter);
            if (data == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
            return new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ObjectContent<IEnumerable<Article>>(data, new System.Net.Http.Formatting.JsonMediaTypeFormatter())
            };
        }

        public HttpResponseMessage GetSearchedData(string query)
        {
            if (query == null)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
            throw new NotImplementedException();
        }

        public IEnumerable<Article> GetArticlesByGroup(string groupId)
        {
            var data = new List<Article>();

            data.AddRange(mModuleContract.LookupArticles(new ArticleFilter() { GroupId = Guid.Parse(groupId) }));

            return data;
        }

        public IEnumerable<Article> GetArticlesByTag(string tag)
        {
            var data = new List<Article>();

            data.AddRange(mModuleContract.LookupArticles(new ArticleFilter() { Tags = new string[] { tag } }));

            return data;
        }

        [Authorize]
        public IEnumerable<Article> GetArticlesByAccountDefaults(Guid userId)
        {
            var data = new List<Article>();

            if (!Guid.Empty.Equals(userId))
            {
                var profile = mModuleContract.GetUserProfile(userId);

                if (profile != null)
                {
                    IEnumerable<string> tagList = new string[0];
                    if (profile.Tags != null)
                        tagList = profile.Tags.Split(new char[] { '\r' }, StringSplitOptions.RemoveEmptyEntries);

                    var filter = new ArticleFilter()
                    {
                        Tags = tagList
                    };
                    data.AddRange(mModuleContract.LookupArticles(filter));
                }
            }
            return data;
        }


        private static readonly INewsPaperModuleContract mModuleContract = null;

    }
}
