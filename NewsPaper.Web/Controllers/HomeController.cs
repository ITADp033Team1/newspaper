﻿using NewsPaperWeb.Filters;
using SoftServe.Modules.NewsPaperExtensions;
using SoftServe.Modules.NewsPaperExtensions.Contracts;
using SoftServe.Modules.NewsPaperExtensions.Models;
using SoftServe.NewsPaperWeb.Filters;
using SoftServe.NewsPaperWeb.ViewModels;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace NewsPaperWeb.Controllers
{
    [RedirectFilter]
    [LocalizationAware]
    public class HomeController : Controller
    {
        static HomeController()
        {
            mModuleContract = LogicContainer.Instance.GetService<INewsPaperModuleContract>();
        }

        public ActionResult Index()
        {
            var template = "{0}...";
            var data = mModuleContract.LookupArticles();
            AutoMapper.Mapper.CreateMap<Article, ArticleTileViewModel>().ForMember( tile => tile.Content, 
                map => map.MapFrom(
                    scr => string.Format(template, scr.Content.Substring(0, 200)))).ForMember(tile => tile.Title,
                map => map.MapFrom(
                    scr => string.Format(template, scr.Content.Substring(0, 50))));

            var viewData = new List<ArticleTileViewModel>();
            foreach (var item in data)
            {
                viewData.Add(AutoMapper.Mapper.Map<ArticleTileViewModel>(item));
            }
            return View(viewData);
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult ViewArticle(string id)
        {
            var articleId = default(Guid);
            if (!Guid.TryParse(id, out articleId))
            {
                return new RedirectResult("~/Errors/404");
            }
            
            var article = mModuleContract.View(articleId);

            if (article == default(Article))
                throw new KeyNotFoundException();
            AutoMapper.Mapper.CreateMap<Article, ArticleViewModel>();
            var aView = AutoMapper.Mapper.Map<ArticleViewModel>(article);

            return View(aView);
        }

      
        public ActionResult SetLanguage(string id, string returnUrl = null)
        {
            id = id == "ru" ? "ru" : "en";
            Response.AppendCookie(LocalizationAwareAttribute.createCookie(id));
            return Redirect(returnUrl);
        }

        #region Helpers
        internal ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        #endregion


        private static readonly INewsPaperModuleContract mModuleContract = null;

    }
}
