﻿using SoftServe.Modules.NewsPaperExtensions;
using SoftServe.Modules.NewsPaperExtensions.Abstractions;
using SoftServe.Modules.NewsPaperExtensions.Contracts;
using SoftServe.Modules.NewsPaperExtensions.Models;
using SoftServe.NewsPaperWeb.Filters;
using SoftServe.NewsPaperWeb.Models;
using SoftServe.NewsPaperWeb.ViewModels;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SoftServe.NewsPaperWeb.Controllers
{
    [RedirectFilter]
    [LocalizationAwareAttribute]
    public class BackendController : Controller
    {
        const string ErrorURL = "~/Errors/Http400";

        static BackendController()
        {
            /// Register mappings
            AutoMapper.Mapper.CreateMap<Article, ArticleViewModel>();
            AutoMapper.Mapper.CreateMap<ArticleViewModel, Article>();

            mModuleContract = LogicContainer.Instance.GetService<INewsPaperModuleContract>();
        }

        //
        // GET: /Backend/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetRequests(DefaultSearchViewModel filter)
        {
            throw new NotImplementedException();
        }

        public ActionResult GetUserProfile(Guid id)
        {
            if (!Guid.Empty.Equals(id))
            {
                var profile = mModuleContract.GetUserProfile(id);
                var profileview = Map<ProfileBase, UserProfileViewModel>(profile);
                return View(profileview);
            }
            return Redirect(ErrorURL);
        }


        public ActionResult ResetUserPassword(Guid id)
        {
            if (!Guid.Empty.Equals(id))
            {
                mModuleContract.ResetUserPassword(id);

                ///TODO: Add custom logic to send data to email
                return View();
            }

            throw new KeyNotFoundException();
        }

        public ActionResult GetArticleHistory(Guid articleId)
        {
            var articlesHistory = new List<ArticleHistory>();
            if (!Guid.Empty.Equals(articleId))
            {
                articlesHistory = mModuleContract.GetArticleHistory(articleId) as List<ArticleHistory>;
                return View(articlesHistory);
            }

            throw new KeyNotFoundException();
        }

        public ActionResult AddCommentToObject(Guid objectId, CommentViewModel commentview)
        {
            if (!Guid.Empty.Equals(objectId) && commentview != null)
            {
                Comment comment = Map<CommentViewModel, Comment>(commentview);
                mModuleContract.AddCommentToObject(objectId, comment);
                return View();
            }

            return Redirect(ErrorURL);
        }

        public ActionResult GetComments(Guid articleId, Guid ownerId)
        {
            if (!Guid.Empty.Equals(articleId) && !Guid.Empty.Equals(ownerId))
            {
                mModuleContract.GetComments(articleId, ownerId);
                return View();
            }

            return Redirect(ErrorURL);
        }

        //
        // GET: /Backend/NewArticle

        public ActionResult NewArticle()
        {
            return View( );
        }

        [HttpPost]
        public ActionResult NewArticle(ArticleViewModel article)
        {
            if (article != null)
            {
                var moduleArticle = AutoMapper.Mapper.Map<Article>(article);

                var dArticle = mModuleContract.NewArticle(moduleArticle);

                /// Convert Domain to view  model
                /// 
                article = AutoMapper.Mapper.Map<ArticleViewModel>(dArticle);

                /// 
                /// Return view model
                return View(article);
            }

            throw new ArgumentException("acticle");
        }

        public ActionResult GetUsers(DefaultSearchViewModel filter)
        {           
            throw new NotImplementedException();
        }


        //
        // POST: /Backend/

        [HttpPost]
        public ActionResult ApproveArticle(Guid articleId, string returnUrl = null)
        {
            if (Guid.Empty != articleId)
            {
                mModuleContract.ApproveArticle(articleId);
                return View();
            }

            throw new KeyNotFoundException();
        }

        [HttpPost]
        public ActionResult DeclineArticle(Guid articleId, string comment, string returnUrl = null)
        {
            if (string.IsNullOrEmpty(comment))
                throw new ArgumentException("comment");

            if (articleId != Guid.Empty)
            {
                mModuleContract.DeclineArticle(articleId, comment);
                return RedirectToLocal(returnUrl);
            }

            throw new KeyNotFoundException();
        }

        [HttpPost]
        public ActionResult DeleteUserProfile(Guid id, string returnUrl = null)
        {
            if (!Guid.Empty.Equals(id))
            {
                mModuleContract.DeleteUserProfile(id);
                return RedirectToLocal(returnUrl);
            }

            throw new KeyNotFoundException("user profile");
        }

        [HttpPost]
        public ActionResult SetUserProfileState(Guid id, eUserAccountState newState, string returnUrl = null)
        {
            if (!Guid.Empty.Equals(id))
            {
                mModuleContract.SetUserProfileState(id, newState);
                return RedirectToLocal(returnUrl);
            }

            throw new NotSupportedException();
        }

        [HttpPost]
        public ActionResult ApproveModeratorAccount(string login, string returnUrl = null)
        {
            if (!string.IsNullOrEmpty(login))
            {
                mModuleContract.ApproveModeratorAccount(login);
                return RedirectToLocal(returnUrl);
            }


            throw new KeyNotFoundException();
        }

        [HttpPost]
        public ActionResult DeclineModeratorAccount(string login, string returnUrl = null)
        {
            if (!string.IsNullOrEmpty(login))
            {
                mModuleContract.DeclineModeratorAccount(login);
                return View();
            }

            throw new KeyNotFoundException();
        }

        [HttpPost]
        public ActionResult Update(ArticleViewModel articleview, string returnUrl = null)
        {
            if (articleview != null)
            {
                Article article = AutoMapper.Mapper.Map<Article>(articleview);
                mModuleContract.Update(article);
                return View();
            }
            throw new KeyNotFoundException();
        }

        [HttpPost]
        public ActionResult SendRequestToDelete(Guid articleId, string returnUrl = null)
        {
            if (!Guid.Empty.Equals(articleId))
            {
                mModuleContract.SendRequestToDelete(articleId);
                return View();
            }

            throw new KeyNotFoundException();
        }

        [HttpPost]
        public ActionResult DeleteArticle(Guid articleId, string returnUrl = null)
        {
            if (!Guid.Empty.Equals(articleId))
            {
                mModuleContract.DeleteArticle(articleId);
                return View();
            }

            throw new KeyNotFoundException();
        }


        #region Helpers

        [NonAction]
        public TDestination Map<TInstance, TDestination>(TInstance instance) where TDestination : class
        {
            if (instance != null)
            {

                return AutoMapper.Mapper.Map<TDestination>(instance) as TDestination;
            }
            return null;
        }

        internal ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        #endregion

        private static readonly INewsPaperModuleContract mModuleContract = null;
    }
}
