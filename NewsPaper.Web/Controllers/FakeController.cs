﻿using SoftServe.NewsPaperWeb.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SoftServe.NewsPaperWeb.Controllers
{

    [RedirectFilter]
    public class FakeController : Controller
    {
        //
        // GET: /Fake/

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult NotImplemented()
        {
            throw new NotImplementedException();
        }

        public ActionResult KeyNotFound()
        {
            throw new KeyNotFoundException();
        }
        public ActionResult IO()
        {
            throw new System.IO.IOException();
        }
    }
}
