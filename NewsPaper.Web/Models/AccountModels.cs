﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
using SoftServe.NewsPaperWeb.Attributes;
using SoftServe.NewsPaperWeb.Content;
using WebMatrix.WebData;

namespace SoftServe.NewsPaperWeb.Models
{
    public class UsersContext : DbContext
    {
        public UsersContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
    }

    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
    }

    public class RegisterExternalLoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        public string ExternalLoginData { get; set; }
    }

    public class LocalPasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        /// <summary>
        /// User Login
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(LanguageResource),
                  ErrorMessageResourceName = "OnLoginIsRequired", AllowEmptyStrings = false)]
        [StringLength(30, ErrorMessageResourceType = typeof(LanguageResource),
            ErrorMessageResourceName = "LoginLengthError", MinimumLength = 4)]
        [Display(ResourceType = typeof(LanguageResource), Name = "Login")]
        public string UserName { get; set; }

        /// <summary>
        /// User password
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(LanguageResource),
            ErrorMessageResourceName = "OnPasswordIsRequired", AllowEmptyStrings = false)]
        [StringLength(20, ErrorMessageResourceType = typeof(LanguageResource),
           ErrorMessageResourceName = "PasswordLengthError", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(LanguageResource), Name = "Password")]
        public string Password { get; set; }


        /// <summary>
        /// Keep LogIn
        /// </summary>
        [System.ComponentModel.DefaultValue(true)]
        [Display(ResourceType = typeof(LanguageResource), Name = "RememberMe")]   
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
        /// <summary>
        /// User Login
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(LanguageResource),
                  ErrorMessageResourceName = "OnLoginIsRequired", AllowEmptyStrings = false)]
        [StringLength(30, ErrorMessageResourceType = typeof(LanguageResource),
            ErrorMessageResourceName = "LoginLengthError", MinimumLength = 6)]
        [Display(ResourceType = typeof(LanguageResource), Name = "Login")]
        [RegularExpression("[a-zA-Z]+", ErrorMessageResourceType = typeof(LanguageResource),
            ErrorMessageResourceName = "LoginFormatError")]
        [DataType(DataType.Text)]
        public string UserName { get; set; }

        [Required(ErrorMessageResourceType = typeof(LanguageResource),
            ErrorMessageResourceName = "OnPasswordIsRequired", AllowEmptyStrings = false)]
        [StringLength(20, ErrorMessageResourceType = typeof(LanguageResource),
           ErrorMessageResourceName = "PasswordLengthError", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(LanguageResource), Name = "Password")]
        public string Password { get; set; }


        /// <summary>
        /// User public email
        /// </summary>
        [Display(ResourceType = typeof(LanguageResource), Name = "PublicEmail")]
        [EmailValidation(ErrorMessageResourceType = typeof(LanguageResource),
                  ErrorMessageResourceName = "EmailFormatError")]
        [DataType(DataType.Password)]
        public string PublicEmail
        {
            get;
            set;
        }
        /// <summary>
        /// User password confirm
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(LanguageResource),
            ErrorMessageResourceName = "OnPasswordConfirmIsRequired", AllowEmptyStrings = false)]
        [Compare("Password", ErrorMessageResourceType = typeof(LanguageResource),
           ErrorMessageResourceName = "OnPasswordConfirmFail")]
        [Display(ResourceType = typeof(LanguageResource), Name = "PasswordConfirm")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
    }

    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }
}
