﻿//<copyright file="UserProfileViewModel.cs" company="SoftServe">
//Copyright (c) 2013 All Rights Reserved
//</copyright>
//<author>Sklyarskiy Alexander</author>
//<date>3/23/2013 3:44:27 PM</date>
//<summary>TODO: Update summary</summary>

using SoftServe.NewsPaperWeb.Attributes;
using SoftServe.NewsPaperWeb.Content;
using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SoftServe.NewsPaperWeb.ViewModels
{
    /// <summary>
    /// Class for user profiles.
    /// </summary>
    public class UserProfileViewModel
    {
        #region Properties
        
        /// <summary>
        /// User Id
        /// </summary>
        [HiddenInput( DisplayValue = false )]
        public Guid Id
        {
            get;
            set;
        }

        /// <summary>
        /// User Login
        /// </summary>
        [Editable( false )]
        [Display( ResourceType = typeof( LanguageResource ), Name = "Login" )]
        public string Login
        {
            get;
            set;
        }

        /// <summary>
        /// User Email
        /// </summary>
        [Required( ErrorMessageResourceType = typeof( LanguageResource ),
                  ErrorMessageResourceName = "OnEmailIsRequired", AllowEmptyStrings = false )]
        [EmailValidation( ErrorMessageResourceType = typeof( LanguageResource ),
                  ErrorMessageResourceName = "EmailFormatError" )]
        [Display( ResourceType = typeof( LanguageResource ), Name = "Email" )]
        [DataType( DataType.EmailAddress )]
        public string Email
        {
            get;
            set;
        }

        /// <summary>
        /// User Picture
        /// </summary>
        [Display( ResourceType = typeof( LanguageResource ), Name = "ProfileImage" )]
        [DataType( DataType.ImageUrl )]
        public string Picture
        {
            get;
            set;
        }

        /// <summary>
        /// User favorite tags
        /// </summary>
        [DataType( DataType.MultilineText )]
        [Display( ResourceType = typeof( LanguageResource ), Name = "FavoriteTags" )]
        public string Tags
        {
            get;
            set;
        }

        #endregion
    }
}