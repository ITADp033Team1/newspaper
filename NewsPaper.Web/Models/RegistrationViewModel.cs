﻿//<copyright file="RegistrationViewModel.cs" company="SoftServe">
//Copyright (c) 2013 All Rights Reserved
//</copyright>
//<author>Sklyarskiy Alexander</author>
//<date>3/23/2013 2:25:30 PM</date>
//<summary>TODO: Update summary</summary>

using SoftServe.NewsPaperWeb.Attributes;
using SoftServe.NewsPaperWeb.Content;
using System.ComponentModel.DataAnnotations;

namespace SoftServe.NewsPaperWeb.ViewModels
{
    /// <summary>
    /// Registration view model
    /// </summary>
    public class RegistrationViewModel
    {
        #region Properties

        /// <summary>
        /// User Login
        /// </summary>
        [Required( ErrorMessageResourceType = typeof( LanguageResource ),
                  ErrorMessageResourceName = "OnLoginIsRequired", AllowEmptyStrings = false )]
        [StringLength( 30, ErrorMessageResourceType = typeof( LanguageResource ),
            ErrorMessageResourceName = "LoginLengthError", MinimumLength = 6 )]
        [Display( ResourceType = typeof( LanguageResource ), Name = "Login" )]
        [RegularExpression( "[a-zA-Z]+", ErrorMessageResourceType = typeof( LanguageResource ),
            ErrorMessageResourceName = "LoginFormatError" )]
        [DataType( DataType.Text )]
        public string Login
        {
            get;
            set;
        }

        /// <summary>
        /// User Email
        /// </summary>
        [Required( ErrorMessageResourceType = typeof( LanguageResource ),
                  ErrorMessageResourceName = "OnEmailIsRequired", AllowEmptyStrings = false )]
        [EmailValidation( ErrorMessageResourceType = typeof( LanguageResource ),
                  ErrorMessageResourceName = "EmailFormatError" )]
        [Display( ResourceType = typeof( LanguageResource ), Name = "Email" )]
        [DataType( DataType.EmailAddress )]
        public string Email
        {
            get;
            set;
        }

        /// <summary>
        /// User password
        /// </summary>
        [Required( ErrorMessageResourceType = typeof( LanguageResource ),
            ErrorMessageResourceName = "OnPasswordIsRequired", AllowEmptyStrings = false )]
        [StringLength( 20, ErrorMessageResourceType = typeof( LanguageResource ),
           ErrorMessageResourceName = "PasswordLengthError", MinimumLength = 8 )]
        [DataType( DataType.Password )]
        [Display( ResourceType = typeof( LanguageResource ), Name = "Password" )]
        public string Password
        {
            get;
            set;
        }

        /// <summary>
        /// User password confirm
        /// </summary>
        [Required( ErrorMessageResourceType = typeof( LanguageResource ),
            ErrorMessageResourceName = "OnPasswordConfirmIsRequired", AllowEmptyStrings = false )]
        [Compare( "Password", ErrorMessageResourceType = typeof( LanguageResource ),
           ErrorMessageResourceName = "OnPasswordConfirmFail" )]
        [Display( ResourceType = typeof( LanguageResource ), Name = "PasswordConfirm" )]
        [DataType( DataType.Password )]
        public string ConfirmPassword
        {
            get;
            set;
        }

        /// <summary>
        /// User public email
        /// </summary>
        [Display( ResourceType = typeof( LanguageResource ), Name = "PublicEmail" )]
        [EmailValidation( ErrorMessageResourceType = typeof( LanguageResource ),
                  ErrorMessageResourceName = "EmailFormatError" )]
        [DataType( DataType.Password )]
        public string PublicEmail
        {
            get;
            set;
        }

        #endregion
    }
}