﻿//<copyright file="CommentViewModel.cs" company="SoftServe">
//Copyright (c) 2013 All Rights Reserved
//</copyright>
//<author>Sklyarskiy Alexander</author>
//<date>3/24/2013 8:53:29 PM</date>
//<summary>TODO: Update summary</summary>

using SoftServe.NewsPaperWeb.Content;
using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SoftServe.NewsPaperWeb.ViewModels
{
    /// <summary>
    /// Comment class
    /// </summary>
    public class CommentViewModel
    {
        #region Properties
        
        /// <summary>
        /// Comment Id
        /// </summary>
        [HiddenInput( DisplayValue = false )]    
        public Guid Id
        {
            get;
            set;
        }

        /// <summary>
        /// Comment publisher Id
        /// </summary>
        [HiddenInput( DisplayValue = false )]    
        public Guid PublisherID
        {
            get;
            set;
        }

        /// <summary>
        /// Is root comment
        /// </summary>
        [HiddenInput( DisplayValue = false )]    
        public bool IsRoot
        {
            get;
            set;
        }

        /// <summary>
        /// Comment parent Id
        /// </summary>
        [HiddenInput( DisplayValue = false )]    
        public Guid ParentId
        {
            get;
            set;
        }

        /// <summary>
        /// Comment rating
        /// </summary>
        [Editable( false )]
        [Display( ResourceType = typeof( LanguageResource ), Name = "Rating" )]
        public int Rate
        {
            get;
            set;
        }

        /// <summary>
        /// Comment text
        /// </summary>
        [Display( ResourceType = typeof( LanguageResource ), Name = "Comment" )]
        [DataType(DataType.MultilineText)]
        [StringLength( 500, ErrorMessageResourceType = typeof( LanguageResource ),
               ErrorMessageResourceName = "CommentLengthError", MinimumLength = 0 )]
        public string Text
        {
            get;
            set;
        }

        #endregion
    }
}