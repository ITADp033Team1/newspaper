﻿using SoftServe.NewsPaperWeb.ViewModels;
using System;
using System.Collections.Generic;

namespace SoftServe.NewsPaperWeb.Models
{
    public class MainPageModel
    {
        public Guid UserID { get; set; }
        
        public string UserFIO { get; set; }

        public Guid SelectedGroup { get; set; }

        public IEnumerable<string> Tags { get; set; }

        /// <summary>
        /// Длина поля Content содержит только первые 250 символов
        /// Длина самой коллекции 7 статей
        /// </summary>
        public IEnumerable <ArticleViewModel> FirstArticles{ get; set; }
    }
}