﻿//<copyright file="LoginViewModel.cs" company="SoftServe">
//Copyright (c) 2013 All Rights Reserved
//</copyright>
//<author>Sklyarskiy Alexander</author>
//<date>3/23/2013 4:31:03 PM</date>
//<summary>TODO: Update summary</summary>

using SoftServe.NewsPaperWeb.Content;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SoftServe.NewsPaperWeb.ViewModels
{
    /// <summary>
    /// Login view model class.
    /// </summary>
    public class LoginViewModel
    {
        #region MyRegion

        /// <summary>
        /// User Login
        /// </summary>
        [Required( ErrorMessageResourceType = typeof( LanguageResource ),
                  ErrorMessageResourceName = "OnLoginIsRequired", AllowEmptyStrings = false )]
        [StringLength( 30, ErrorMessageResourceType = typeof( LanguageResource ),
            ErrorMessageResourceName = "LoginLengthError", MinimumLength = 6 )]
        [Display( ResourceType = typeof( LanguageResource ), Name = "Login" )]
        [RegularExpression( "[a-zA-Z]+", ErrorMessageResourceType = typeof( LanguageResource ),
            ErrorMessageResourceName = "LoginFormatError" )]
        [DataType( DataType.Text )]
        public string Login
        {
            get;
            set;
        }

        /// <summary>
        /// User password
        /// </summary>
        [Required( ErrorMessageResourceType = typeof( LanguageResource ),
            ErrorMessageResourceName = "OnPasswordIsRequired", AllowEmptyStrings = false )]
        [StringLength( 20, ErrorMessageResourceType = typeof( LanguageResource ),
           ErrorMessageResourceName = "PasswordLengthError", MinimumLength = 8 )]
        [DataType( DataType.Password )]
        [Display( ResourceType = typeof( LanguageResource ), Name = "Password" )]
        public string Password
        {
            get;
            set;
        }

        /// <summary>
        /// Keep LogIn
        /// </summary>
        [DefaultValue(false)]
        [Display( ResourceType = typeof( LanguageResource ), Name = "RememberMe" )]        
        public bool RememberMe
        {
            get;
            set;
        }
        
        #endregion
    }
}