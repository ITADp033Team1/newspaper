﻿//<copyright file="ArticleViewModel.cs" company="SoftServe">
//Copyright (c) 2013 All Rights Reserved
//</copyright>
//<author>Sklyarskiy Alexander</author>
//<date>3/23/2013 2:55:10 PM</date>
//<summary>TODO: Update summary</summary>

using SoftServe.NewsPaperWeb.Content;
using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SoftServe.NewsPaperWeb.ViewModels
{
    /// <summary>
    /// Article view model class.
    /// </summary>
    public class ArticleTileViewModel
    {
        #region Properties
        
        /// <summary>
        /// Article Id
        /// </summary>
        [HiddenInput( DisplayValue = false )]
        public Guid Id
        {
            get;
            set;
        }

        /// <summary>
        /// Article group Id
        /// </summary>
        [HiddenInput(DisplayValue = false)]
        public Guid GroupId
        {
            get;
            set;
        }

        /// <summary>
        /// Article title
        /// </summary>
        [Required( ErrorMessageResourceType = typeof( LanguageResource ),
                   ErrorMessageResourceName = "ArticleTitleIsRequired", AllowEmptyStrings = false )]
        [Display( ResourceType = typeof( LanguageResource ), Name = "ArticleTitle" )]
        [StringLength( 50, ErrorMessageResourceType = typeof( LanguageResource ),
                   ErrorMessageResourceName = "LoginLengthError", MinimumLength = 8 )]
        [RegularExpression( "[a-zA-Z]+", ErrorMessageResourceType = typeof( LanguageResource ),
                   ErrorMessageResourceName = "ArticleTitleFormatError" )]
        [DataType( DataType.Text )]
        public string Title
        {
            get;
            set;
        }

        /// <summary>
        /// Article content
        /// </summary>
        [Required( ErrorMessageResourceType = typeof( LanguageResource ),
                   ErrorMessageResourceName = "ArticleContentIsRequired", AllowEmptyStrings = false )]
        [StringLength( 500, ErrorMessageResourceType = typeof( LanguageResource ),
                   ErrorMessageResourceName = "ArticleContentLengthError", MinimumLength = 50 )]
        [DataType( DataType.MultilineText )]
        public string Content
        {
            get;
            set;
        }

        /// <summary>
        /// Article picture
        /// </summary>
        [StringLength( 100, ErrorMessageResourceType = typeof( LanguageResource ),
                   ErrorMessageResourceName = "ArticleContentLengthError", MinimumLength = 10 )]
        [Display( ResourceType = typeof( LanguageResource ), Name = "ArticleImage" )]
        [DataType( DataType.ImageUrl )]
        public string PictureUrl
        {
            get;
            set;
        }
        #endregion
    }
}