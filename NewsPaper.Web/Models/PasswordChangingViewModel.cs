﻿using SoftServe.NewsPaperWeb.Content;
using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;


namespace SoftServe.NewsPaperWeb.ViewModels
{
    public class PasswordChangingViewModel
    {
        #region Properties

        /// <summary>
        /// User Id
        /// </summary>
        [HiddenInput( DisplayValue = false )]
        public Guid Id
        {
            get;
            set;
        }

        /// <summary>
        /// User old password
        /// </summary>
        [HiddenInput( DisplayValue = false )]
        public string Password
        {
            get;
            set;
        }

        /// <summary>
        /// User old password confirm
        /// </summary>
        [Required( ErrorMessageResourceType = typeof( LanguageResource ),
            ErrorMessageResourceName = "OnPasswordConfirmIsRequired", AllowEmptyStrings = false )]
        [System.Web.Mvc.Compare( "Password", ErrorMessageResourceType = typeof( LanguageResource ),
           ErrorMessageResourceName = "OldPasswordCompareError" )]
        [Display( ResourceType = typeof( LanguageResource ), Name = "OldPassword" )]
        [DataType( DataType.Password )]
        public string ConfirmOldPassword
        {
            get;
            set;
        }

        /// <summary>
        /// User new password
        /// </summary>
        [Required( ErrorMessageResourceType = typeof( LanguageResource ),
            ErrorMessageResourceName = "OnPasswordIsRequired", AllowEmptyStrings = false )]
        [StringLength( 20, ErrorMessageResourceType = typeof( LanguageResource ),
           ErrorMessageResourceName = "PasswordLengthError", MinimumLength = 8 )]
        [DataType( DataType.Password )]
        [Display( ResourceType = typeof( LanguageResource ), Name = "NewPassword" )]
        public string NewPassword
        {
            get;
            set;
        }

        /// <summary>
        /// User new password confirm
        /// </summary>
        [Required( ErrorMessageResourceType = typeof( LanguageResource ),
            ErrorMessageResourceName = "OnPasswordConfirmIsRequired", AllowEmptyStrings = false )]
        [System.Web.Mvc.Compare( "NewPassword", ErrorMessageResourceType = typeof( LanguageResource ),
           ErrorMessageResourceName = "OnPasswordConfirmFail" )]
        [Display( ResourceType = typeof( LanguageResource ), Name = "NewPasswordConfirm" )]
        [DataType( DataType.Password )]
        public string ConfirmNewPassword
        {
            get;
            set;
        }
        #endregion
    }
}