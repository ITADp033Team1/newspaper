﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SoftServe.NewsPaperWeb.Models
{

    public class DefaultSearchViewModel
    {
        public string Tags { get; set; }

        public string Group { get; set; }

        public DateTime? From { get; set; }

        public DateTime? To { get; set; }

        public int TotalItemsAtPage { get; set; }

        public int StartItem { get; set; }
    }
}