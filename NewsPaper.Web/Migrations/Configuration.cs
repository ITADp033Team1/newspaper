namespace SoftServe.NewsPaperWeb.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Web.Security;
    using SoftServe.NewsPaperWeb.Models;
    using WebMatrix.WebData;

    internal sealed class Configuration : DbMigrationsConfiguration<SoftServe.NewsPaperWeb.Models.UsersContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(SoftServe.NewsPaperWeb.Models.UsersContext context)
        {
            WebSecurity.InitializeDatabaseConnection(
                "DefaultConnection",
                "UserProfile",
                "UserId",
                "UserName", autoCreateTables: true);

            if (!Roles.RoleExists("Administrator"))
                Roles.CreateRole("Administrator");

            if (!Roles.RoleExists("Moderator"))
                Roles.CreateRole("Moderator");

            if (!Roles.RoleExists("Publisher"))
                Roles.CreateRole("Publisher");

            if (!WebSecurity.UserExists("root"))
                WebSecurity.CreateUserAndAccount(
                    "root",
                    "password",
                    new { Email = "root@root.root" });

            if (!Roles.GetRolesForUser("root").Contains("Administrator"))
                Roles.AddUsersToRoles(new[] { "root" }, new[] { "Administrator" });
        }
    }
}
