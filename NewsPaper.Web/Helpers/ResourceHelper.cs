﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Resources;
using System.Globalization;

namespace NewsPaperWeb.Helpers
{
    /// <summary>
    /// Helps manage resources and localization.
    /// </summary>
    static public class ResourceHelper
    {
        /// <summary>
        /// Get resource by id string.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string GetResourceById(string id)
        {
            return mResourceManager.GetString(id);
        }
        private static ResourceManager mResourceManager = new ResourceManager(typeof(global::SoftServe.NewsPaperWeb.Content.LanguageResource));
    }


}