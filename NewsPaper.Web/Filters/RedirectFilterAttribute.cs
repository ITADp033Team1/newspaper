﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SoftServe.NewsPaperWeb.Filters
{
    public class RedirectFilterAttribute : FilterAttribute, IExceptionFilter
    {
        public RedirectFilterAttribute()
        {

        }

        public void OnException(ExceptionContext filterContext)
        {
            if (filterContext.ExceptionHandled)
            {
                return;
            }
            if (filterContext.Exception is NotImplementedException)
            {
                filterContext.Result = new RedirectResult("/Errors/Http501");
                filterContext.ExceptionHandled = true;
                return;
            }
            if (filterContext.Exception is System.IO.IOException)
            {
                filterContext.Result = new RedirectResult("/Errors/Http404");
                filterContext.ExceptionHandled = true;
                return;
            }

            if (filterContext.Exception is InvalidOperationException)
            {
                filterContext.Result = new RedirectResult("/Errors/Http401");
                filterContext.ExceptionHandled = true;
                return;
            }

            filterContext.Result = new RedirectResult("/Errors/Http500");
            filterContext.ExceptionHandled = true;
            return;

        }
    }
}