﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SoftServe.Modules.NewsPaperExtensions.Abstractions;
using SoftServe.Modules.NewsPaperExtensions.Models;

namespace SoftServe.NewsPaperWeb.Filters
{
    /// <summary>
    /// Filter for articles by Tags & groupId
    /// </summary>
    public class ArticleFilter: EntityFilterBase<Article>
    {
        public ArticleFilter()
        {

        }
        /// <summary>
        /// Tags collection.
        /// </summary>
        public IEnumerable<string> Tags { get; set; }
        /// <summary>
        /// Group of articles.
        /// </summary>
        public Guid GroupId { get; set; }
        /// <summary>
        /// The beginning of date range.
        /// </summary>
        public DateTime Date1 { get; set; }
        /// <summary>
        /// The end of date range.
        /// </summary>
        public DateTime Date2 { get; set; }
    }
}