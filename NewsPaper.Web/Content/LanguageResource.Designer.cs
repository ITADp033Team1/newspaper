﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SoftServe.NewsPaperWeb.Content {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class LanguageResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal LanguageResource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("SoftServe.NewsPaperWeb.Content.LanguageResource", typeof(LanguageResource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to About.
        /// </summary>
        public static string About {
            get {
                return ResourceManager.GetString("About", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to All users.
        /// </summary>
        public static string AllUsers {
            get {
                return ResourceManager.GetString("AllUsers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Approved articles.
        /// </summary>
        public static string ApprovedArticles {
            get {
                return ResourceManager.GetString("ApprovedArticles", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Approving date:.
        /// </summary>
        public static string ApprovingDate {
            get {
                return ResourceManager.GetString("ApprovingDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} is required field.
        /// </summary>
        public static string ArticleContentIsRequired {
            get {
                return ResourceManager.GetString("ArticleContentIsRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Article should not be less than {0} and more than {1} characters.
        /// </summary>
        public static string ArticleContentLengthError {
            get {
                return ResourceManager.GetString("ArticleContentLengthError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Article image.
        /// </summary>
        public static string ArticleImage {
            get {
                return ResourceManager.GetString("ArticleImage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Articles.
        /// </summary>
        public static string Articles {
            get {
                return ResourceManager.GetString("Articles", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Articles for moderation.
        /// </summary>
        public static string ArticlesForModeration {
            get {
                return ResourceManager.GetString("ArticlesForModeration", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Title.
        /// </summary>
        public static string ArticleTitle {
            get {
                return ResourceManager.GetString("ArticleTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Article head should not contains “&lt;,&gt;”.
        /// </summary>
        public static string ArticleTitleFormatError {
            get {
                return ResourceManager.GetString("ArticleTitleFormatError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} is required field.
        /// </summary>
        public static string ArticleTitleIsRequired {
            get {
                return ResourceManager.GetString("ArticleTitleIsRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} should be more than {0} and less than {1} characters.
        /// </summary>
        public static string ArticleTitleLengthError {
            get {
                return ResourceManager.GetString("ArticleTitleLengthError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator..
        /// </summary>
        public static string AuthenticationProviderError {
            get {
                return ResourceManager.GetString("AuthenticationProviderError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Author.
        /// </summary>
        public static string Author {
            get {
                return ResourceManager.GetString("Author", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Letters only.
        /// </summary>
        public static string AuthorFormatError {
            get {
                return ResourceManager.GetString("AuthorFormatError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} is required field.
        /// </summary>
        public static string AuthorIsRequired {
            get {
                return ResourceManager.GetString("AuthorIsRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} should be more than {0} and less than {1} characters.
        /// </summary>
        public static string AuthorLengthError {
            get {
                return ResourceManager.GetString("AuthorLengthError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Author login.
        /// </summary>
        public static string AuthorLogin {
            get {
                return ResourceManager.GetString("AuthorLogin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Back.
        /// </summary>
        public static string Back {
            get {
                return ResourceManager.GetString("Back", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bad request.
        /// </summary>
        public static string BadRequest {
            get {
                return ResourceManager.GetString("BadRequest", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Blocked users.
        /// </summary>
        public static string BlockedUsers {
            get {
                return ResourceManager.GetString("BlockedUsers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Block user.
        /// </summary>
        public static string Blockuser {
            get {
                return ResourceManager.GetString("Blockuser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Copyright.
        /// </summary>
        public static string c {
            get {
                return ResourceManager.GetString("c", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Comment.
        /// </summary>
        public static string Comment {
            get {
                return ResourceManager.GetString("Comment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} should be less than {1} characters.
        /// </summary>
        public static string CommentLengthError {
            get {
                return ResourceManager.GetString("CommentLengthError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirm password.
        /// </summary>
        public static string ConfirmPassword {
            get {
                return ResourceManager.GetString("ConfirmPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create Article.
        /// </summary>
        public static string CreateArticle {
            get {
                return ResourceManager.GetString("CreateArticle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Decline article.
        /// </summary>
        public static string Decline {
            get {
                return ResourceManager.GetString("Decline", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Delete.
        /// </summary>
        public static string Delete {
            get {
                return ResourceManager.GetString("Delete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email.
        /// </summary>
        public static string Email {
            get {
                return ResourceManager.GetString("Email", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to A user name for that e-mail address already exists. Please enter a different e-mail address..
        /// </summary>
        public static string EmailExists {
            get {
                return ResourceManager.GetString("EmailExists", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Not valid email format.
        /// </summary>
        public static string EmailFormatError {
            get {
                return ResourceManager.GetString("EmailFormatError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email length should be more than {1} and less than {2} characters.
        /// </summary>
        public static string EmailLengthError {
            get {
                return ResourceManager.GetString("EmailLengthError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The e-mail address provided is invalid. Please check the value and try again..
        /// </summary>
        public static string EmailProvidedInvalid {
            get {
                return ResourceManager.GetString("EmailProvidedInvalid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to English.
        /// </summary>
        public static string English {
            get {
                return ResourceManager.GetString("English", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error 400.
        /// </summary>
        public static string Error400 {
            get {
                return ResourceManager.GetString("Error400", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error 401.
        /// </summary>
        public static string Error401 {
            get {
                return ResourceManager.GetString("Error401", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error 404.
        /// </summary>
        public static string Error404 {
            get {
                return ResourceManager.GetString("Error404", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error 500.
        /// </summary>
        public static string Error500 {
            get {
                return ResourceManager.GetString("Error500", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error 501.
        /// </summary>
        public static string Error501 {
            get {
                return ResourceManager.GetString("Error501", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Favorite tags: .
        /// </summary>
        public static string FavoriteTags {
            get {
                return ResourceManager.GetString("FavoriteTags", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Should be less than {1} tags.
        /// </summary>
        public static string FavoriteTagsNumberError {
            get {
                return ResourceManager.GetString("FavoriteTagsNumberError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Changes history.
        /// </summary>
        public static string History {
            get {
                return ResourceManager.GetString("History", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to I have read and agreed with the site rules.
        /// </summary>
        public static string IAgree {
            get {
                return ResourceManager.GetString("IAgree", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Internal server error.
        /// </summary>
        public static string InternalServerError {
            get {
                return ResourceManager.GetString("InternalServerError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Login.
        /// </summary>
        public static string Login {
            get {
                return ResourceManager.GetString("Login", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Only letters allowed.
        /// </summary>
        public static string LoginFormatError {
            get {
                return ResourceManager.GetString("LoginFormatError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Login should be more than {2} and less than {1} characters.
        /// </summary>
        public static string LoginLengthError {
            get {
                return ResourceManager.GetString("LoginLengthError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to LogOut of site.
        /// </summary>
        public static string LogOut {
            get {
                return ResourceManager.GetString("LogOut", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Moderator: .
        /// </summary>
        public static string Moderator {
            get {
                return ResourceManager.GetString("Moderator", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to New Password.
        /// </summary>
        public static string NewPassword {
            get {
                return ResourceManager.GetString("NewPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to New password confirm.
        /// </summary>
        public static string NewPasswordConfirm {
            get {
                return ResourceManager.GetString("NewPasswordConfirm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Not found.
        /// </summary>
        public static string NotFound {
            get {
                return ResourceManager.GetString("NotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Not implemented.
        /// </summary>
        public static string NotImplemented {
            get {
                return ResourceManager.GetString("NotImplemented", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Number of views.
        /// </summary>
        public static string NumberOfViews {
            get {
                return ResourceManager.GetString("NumberOfViews", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Old password.
        /// </summary>
        public static string OldPassword {
            get {
                return ResourceManager.GetString("OldPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Does not match with old password.
        /// </summary>
        public static string OldPasswordCompareError {
            get {
                return ResourceManager.GetString("OldPasswordCompareError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email is required field.
        /// </summary>
        public static string OnEmailIsRequired {
            get {
                return ResourceManager.GetString("OnEmailIsRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} is required field.
        /// </summary>
        public static string OnLoginIsRequired {
            get {
                return ResourceManager.GetString("OnLoginIsRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Entered passwords don&apos;t match.
        /// </summary>
        public static string OnPasswordConfirmFail {
            get {
                return ResourceManager.GetString("OnPasswordConfirmFail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} is required field.
        /// </summary>
        public static string OnPasswordConfirmIsRequired {
            get {
                return ResourceManager.GetString("OnPasswordConfirmIsRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} is required field.
        /// </summary>
        public static string OnPasswordIsRequired {
            get {
                return ResourceManager.GetString("OnPasswordIsRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Picture size should be less than {0}Kb.
        /// </summary>
        public static string OnPictureSize {
            get {
                return ResourceManager.GetString("OnPictureSize", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Picture URL is less than {0} symbols is not valid.
        /// </summary>
        public static string OnPictureURLIsLessThan {
            get {
                return ResourceManager.GetString("OnPictureURLIsLessThan", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your should choose more than {2} and less than {1} tag(s).
        /// </summary>
        public static string OnTagsError {
            get {
                return ResourceManager.GetString("OnTagsError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pages count.
        /// </summary>
        public static string PagesCount {
            get {
                return ResourceManager.GetString("PagesCount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password.
        /// </summary>
        public static string Password {
            get {
                return ResourceManager.GetString("Password", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The password retrieval answer provided is invalid. Please check the value and try again..
        /// </summary>
        public static string PasswordAnswerInvalid {
            get {
                return ResourceManager.GetString("PasswordAnswerInvalid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password confirm.
        /// </summary>
        public static string PasswordConfirm {
            get {
                return ResourceManager.GetString("PasswordConfirm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The password provided is invalid. Please enter a valid password value..
        /// </summary>
        public static string PasswordInvalid {
            get {
                return ResourceManager.GetString("PasswordInvalid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} should be more than {2} and less than {1} characters.
        /// </summary>
        public static string PasswordLengthError {
            get {
                return ResourceManager.GetString("PasswordLengthError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The password retrieval question provided is invalid. Please check the value and try again..
        /// </summary>
        public static string PasswordQuestionInvalid {
            get {
                return ResourceManager.GetString("PasswordQuestionInvalid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Picture Url should be more than {0} and less than {1} characters.
        /// </summary>
        public static string PictureUrlLengthError {
            get {
                return ResourceManager.GetString("PictureUrlLengthError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Posted for moderation.
        /// </summary>
        public static string PostedForModeration {
            get {
                return ResourceManager.GetString("PostedForModeration", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Profile.
        /// </summary>
        public static string Profile {
            get {
                return ResourceManager.GetString("Profile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Profile Image.
        /// </summary>
        public static string ProfileImage {
            get {
                return ResourceManager.GetString("ProfileImage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Publications.
        /// </summary>
        public static string Publications {
            get {
                return ResourceManager.GetString("Publications", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email.
        /// </summary>
        public static string PublicEmail {
            get {
                return ResourceManager.GetString("PublicEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Published:.
        /// </summary>
        public static string Published {
            get {
                return ResourceManager.GetString("Published", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rating.
        /// </summary>
        public static string Rating {
            get {
                return ResourceManager.GetString("Rating", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Register.
        /// </summary>
        public static string Register {
            get {
                return ResourceManager.GetString("Register", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Register.
        /// </summary>
        public static string RegisterLink {
            get {
                return ResourceManager.GetString("RegisterLink", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to if you don&apos;t have an account..
        /// </summary>
        public static string RegisterLinkMessage {
            get {
                return ResourceManager.GetString("RegisterLinkMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Registration.
        /// </summary>
        public static string Registration {
            get {
                return ResourceManager.GetString("Registration", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Remember Me.
        /// </summary>
        public static string RememberMe {
            get {
                return ResourceManager.GetString("RememberMe", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Russian.
        /// </summary>
        public static string Russian {
            get {
                return ResourceManager.GetString("Russian", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Search.
        /// </summary>
        public static string Search {
            get {
                return ResourceManager.GetString("Search", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Send.
        /// </summary>
        public static string Send {
            get {
                return ResourceManager.GetString("Send", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sign In.
        /// </summary>
        public static string SignIn {
            get {
                return ResourceManager.GetString("SignIn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Start.
        /// </summary>
        public static string Start {
            get {
                return ResourceManager.GetString("Start", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to State: .
        /// </summary>
        public static string State {
            get {
                return ResourceManager.GetString("State", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Status.
        /// </summary>
        public static string Status {
            get {
                return ResourceManager.GetString("Status", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Submit.
        /// </summary>
        public static string Submit {
            get {
                return ResourceManager.GetString("Submit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Symbols count.
        /// </summary>
        public static string SymbolsCount {
            get {
                return ResourceManager.GetString("SymbolsCount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tags:.
        /// </summary>
        public static string Tags {
            get {
                return ResourceManager.GetString("Tags", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hould be less than {1} tags.
        /// </summary>
        public static string TagsError {
            get {
                return ResourceManager.GetString("TagsError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unauthorized.
        /// </summary>
        public static string Unauthorized {
            get {
                return ResourceManager.GetString("Unauthorized", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unblocked users.
        /// </summary>
        public static string UnblockedUsers {
            get {
                return ResourceManager.GetString("UnblockedUsers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unblock user.
        /// </summary>
        public static string Unblockuser {
            get {
                return ResourceManager.GetString("Unblockuser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator..
        /// </summary>
        public static string UnknownErrorOccurred {
            get {
                return ResourceManager.GetString("UnknownErrorOccurred", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator..
        /// </summary>
        public static string UserCreationCanceled {
            get {
                return ResourceManager.GetString("UserCreationCanceled", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Username already exists. Please enter a different username..
        /// </summary>
        public static string UsernameExists {
            get {
                return ResourceManager.GetString("UsernameExists", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The user name provided is invalid. Please check the value and try again..
        /// </summary>
        public static string UsernameProvidedInvalid {
            get {
                return ResourceManager.GetString("UsernameProvidedInvalid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Users.
        /// </summary>
        public static string Users {
            get {
                return ResourceManager.GetString("Users", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User status.
        /// </summary>
        public static string UserStatus {
            get {
                return ResourceManager.GetString("UserStatus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Words count.
        /// </summary>
        public static string WordsCount {
            get {
                return ResourceManager.GetString("WordsCount", resourceCulture);
            }
        }
    }
}
