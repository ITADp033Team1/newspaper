﻿using System.Web;
using System.Web.Mvc;
using SoftServe.NewsPaperWeb.Filters;
using NewsPaperWeb.Filters;

namespace NewsPaperWeb
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new RedirectFilterAttribute());
            filters.Add(new InitializeSimpleMembershipAttribute());
        }
    }
}