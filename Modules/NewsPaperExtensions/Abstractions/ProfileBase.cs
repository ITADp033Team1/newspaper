﻿// <copyright file="UserProfile.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Bilokin Sviatoslav</author>
// <date>3/20/2013 3:03:37 PM</date>
// <summary>TODO: Update summary</summary>
namespace SoftServe.Modules.NewsPaperExtensions.Abstractions
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using System.ComponentModel.DataAnnotations;
    using System.Text;
    using Ocorola.Module.Infrastructure.Models;

    /// <summary>
    /// Abstract profile for users.
    /// </summary>
    [DataContract]
    public abstract class ProfileBase : DomainModelBase
    {
        #region Properties
        /// <summary>
        /// User Id.
        /// </summary>
        [Key]
        [DataMember(Order = 1, IsRequired = true, Name = "id")]
        public Guid Id { get; set; }

        /// <summary>
        /// User Email address.
        /// </summary>
        [DataType(DataType.EmailAddress)]
        [DataMember(Order = 2, IsRequired = true, Name = "email")]
        public string Email { get; set; }
        /// <summary>
        /// User Password.
        /// </summary>
        [DataType(DataType.Password)]
        [DataMember(Order = 3, Name = "password")]
        public string Password { get; set; }

        /// <summary>
        /// User Login.
        /// </summary>
        [DataMember(Order = 4, IsRequired = true, Name = "login")]
        public string Login { get; set; }

        /// <summary>
        /// User Picture.
        /// </summary>
        [DataMember(Order = 5, IsRequired = false, Name = "picture")]
        public string Picture { get; set; }

        /// <summary>
        /// User collection of Tags.
        /// </summary>
        [DataMember(Order = 6, Name = "tags")]
        public string Tags { get; set; }
        #endregion


        #region DomainModelBase implementation


        public override void AssignTo(object obj)
        {
            if (obj is ProfileBase)
            {
                var profile = obj as ProfileBase;

                profile.Id = Id;
                profile.Tags = Tags;
                profile.Email = Email;
                profile.Login = Login;
                profile.Picture = Picture;
                profile.Password = Password;

                return;
            }
            throw new NotSupportedException();
        }

        public override bool IsKeyEquals(object key)
        {
            if (key is ProfileBase)
            {
                var profile = key as ProfileBase;
                return Helpers.KeyHelper.IsGuidKeyEquals(profile.Id, Id);
            }
            return Helpers.KeyHelper.IsGuidKeyEquals(key, Id);
        }

        #endregion
    }
}
