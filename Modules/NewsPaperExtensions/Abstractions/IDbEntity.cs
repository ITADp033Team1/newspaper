﻿// -----------------------------------------------------------------------
// <copyright file="IEntity.cs" company="Soft Serve Academy">
// This code was written by Evgeniy Galenko
// </copyright>
// -----------------------------------------------------------------------
namespace SoftServe.Modules.NewsPaperExtensions.Abstractions
{
    using Ocorola.Entity.Infrastructure;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// 
    /// </summary>
    public interface IDbEntity : IStorageEntity
    {
    }
}