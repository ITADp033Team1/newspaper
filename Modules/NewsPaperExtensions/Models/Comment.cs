﻿// <copyright file="Comment.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Bilokin Sviatoslav</author>
// <date>3/20/2013 3:36:34 PM</date>
// <summary>TODO: Update summary</summary>
namespace SoftServe.Modules.NewsPaperExtensions.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Web;
    using Ocorola.Module.Infrastructure.Models;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    /// <summary>
    /// Comment entity.
    /// </summary>
    [DataContract]
    public class Comment: DomainModelBase
    {
        static Comment()
        {
            DomainModelBase.RegisterType<Comment>();
        }
        #region Properties
        
        /// <summary>
        /// Comment  Id.
        /// </summary>
        [Key]
        [DataMember(Order = 1, IsRequired = true, Name  = "commentId")]
        public Guid Id { get; set; }
        /// <summary>
        /// Article id which contains comment.
        /// </summary>
        [DataMember(Order = 2, IsRequired = true, Name = "commentArticleId")]
        public Guid ArticleId { get; set; }
        /// <summary>
        /// Comment publisher ID.
        /// </summary>
        [DataMember(Order = 3, IsRequired = true, Name = "publisherId")]
        public Guid PublisherID { get; set; }
        
        /// <summary>
        /// ID of parent comment.
        /// </summary>
        [DataMember(Order = 4, IsRequired = true, Name = "parentId")]
        public Guid ParentId { get; set; }
        
        /// <summary>
        /// Indicates, that comment has written for article.
        /// </summary>
        [DataMember(Order = 5, Name = "isRoot")]
        public bool IsRoot { get; set; }
        
        /// <summary>
        /// Comment rating.
        /// </summary>
        [DataMember(Order = 6, Name = "commentRate" )]
        public int Rate { get; set; }
        /// <summary>
        /// Date of creation.
        /// </summary>
        [DataMember(Order = 7, Name = "commentData")]
        public DateTime Date { get; set; }
        /// <summary>
        /// Comment body.
        /// </summary>
        [DataMember(Order = 8, IsRequired = true, Name = "body")]
        public string Text { get; set; }
        #endregion

        public override void AssignTo(object obj)
        {
            if (obj is Comment)
            {
                var comment = obj as Comment;

                comment.Id = Id;
                comment.IsRoot = IsRoot;
                comment.ParentId = ParentId;
                comment.PublisherID = PublisherID;
                comment.Rate = Rate;
                comment.Date = Date;
                comment.ArticleId = ArticleId;
                comment.Text = Text;
                return;
            }
            throw new NotSupportedException();
        }

        public override bool IsKeyEquals( object key )
        {
            if (key is Comment)
            {
                return Helpers.KeyHelper.IsGuidKeyEquals( key, (key as Comment).Id );
            }
            return Helpers.KeyHelper.IsGuidKeyEquals( key, Id );
        }

        public bool Equals(Comment other)
        {
            if (other == null)
                return false;

            if (!other.ArticleId.Equals(ArticleId))
                return false;
            if (!other.Date.Equals(Date))
                return false;
            if (!other.Id.Equals(Id))
                return false;
            if (other.IsRoot != IsRoot)
                return false;
            if (!other.ParentId.Equals(ParentId))
                return false;
            if (!other.PublisherID.Equals(PublisherID))
                return false;
            if (other.Rate != Rate)
                return false;
            if (string.Compare(other.Text, Text) != 0)
                return false;

            return true;
        }
    }
}