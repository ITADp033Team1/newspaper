﻿// <copyright file="eArticleStatus.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>

// <date>3/20/2013 3:36:34 PM</date>
// <summary>TODO: Update summary</summary>
namespace SoftServe.Modules.NewsPaperExtensions.Models
{
    using System;

    /// <summary>
    /// Article status.
    /// </summary>
    public enum eArticleStatus : byte
    {
        New = 0,
        
        Approved,
        
        Declined,

        OnRedaction,
        
        Final
    }
}