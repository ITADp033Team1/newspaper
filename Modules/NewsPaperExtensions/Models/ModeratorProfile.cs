﻿// <copyright file="ModeratorProfile.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Bilokin Sviatoslav</author>
// <date>3/20/2013 4:03:53 PM</date>
// <summary>TODO: Update summary</summary>
namespace SoftServe.Modules.NewsPaperExtensions.Models
{
    using SoftServe.Modules.NewsPaperExtensions.Abstractions;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Runtime.Serialization;
    using System.Text;
    /// <summary>
    /// Moderator profile.
    /// </summary>
    [DataContract]
    public class ModeratorProfile : UserProfile
    {
        static ModeratorProfile()
        {
            ProfileBase.RegisterType<ModeratorProfile>();
        }

        public bool Equals(ModeratorProfile other)
        {
            if (other == null)
                return false;
            return base.Equals(other);
        }
    }
}
