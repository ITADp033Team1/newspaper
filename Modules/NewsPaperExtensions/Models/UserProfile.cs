﻿// <copyright file="SignedUser.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Bilokin Sviatoslav</author>
// <date>3/20/2013 3:36:34 PM</date>
// <summary>TODO: Update summary</summary>
namespace SoftServe.Modules.NewsPaperExtensions.Models
{
    using SoftServe.Modules.NewsPaperExtensions.Abstractions;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using System.Text;
    
    /// <summary>
    /// Signed user's profile.
    /// </summary>
    [DataContract]
    public class UserProfile : ProfileBase
    {
        static UserProfile()
        {
            ProfileBase.RegisterType<UserProfile>();
        }
        /// <summary>
        /// User status.
        /// </summary>
        [DataMember(Order = 1, Name = "UserStatus")]
        public eUserAccountState UserStatus { get; set; }
        /// <summary>
        /// First, last name and a name of father of user.
        /// </summary>
        [DataMember(Order = 2, Name = "UserFIO")]
        public string FIO { get; set; }

        public bool Equals(UserProfile other)
        {
            if (other == null)
                return false;
            if (!base.Equals(other))
                return false;
            if (other.UserStatus != UserStatus)
                return false;
            if (string.Compare(other.FIO, FIO) != 0)
                return false;

            return true;
        }
        
    }
}

