﻿// <copyright file="Article.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Bilokin Sviatoslav</author>
// <date>3/20/2013 3:36:34 PM</date>
// <summary>TODO: Update summary</summary>
namespace SoftServe.Modules.NewsPaperExtensions.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Web;
    using Ocorola.Module.Infrastructure.Models;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Article entity.
    /// </summary>
    [DataContract]
    public class Article : DomainModelBase, IEquatable<Article>
    {
        static Article()
        {
            DomainModelBase.RegisterType<Article>();
        }

        #region Properties
        /// <summary>
        /// Article  Id.
        /// </summary>
        [Key]
        [DataMember(Order = 1, IsRequired = true)]
        public Guid Id { get { return m_Id; } set { m_Id = value; } }
        /// <summary>
        /// Article author.
        /// </summary>
        [DataMember(Order = 2, IsRequired = true, Name = "author")]
        public KeyValuePair<Guid, string> Author
        {
            get;
            set;
        }
        /// <summary>
        /// Article head.
        /// </summary>
        [DataMember(Order = 3, IsRequired = true, Name = "subject")]
        public string Title
        {
            get { return mHead; }
            set { mHead = value; }
        }
        /// <summary>
        /// Article content.
        /// </summary>
        [DataMember(Order = 4, IsRequired = true, Name = "body")]
        public string Content
        {
            get { return mText; }
            set { mText = value; }
        }
        /// <summary>
        /// Status of article.
        /// </summary>
        [DataMember(Order = 5, Name = "status")]
        public eArticleStatus ArticleStatus { get; set; }

        /// <summary>
        /// Picture below the head of article.
        /// </summary>        
        [DataMember(Order = 6, Name = "imageUrl")]
        public string PictureUrl { get; set; }

        /// <summary>
        /// Name of article's moderator.
        /// </summary>
        
        [DataMember(Order = 7, Name = "moderatorInfo")]        
        public KeyValuePair<Guid,string> Moderator { get; set; }
        /// <summary>
        /// Article creation date.
        /// </summary>
        [DataMember(Order = 8, IsRequired = true, Name = "created")]
        public DateTime Updated { get; set; }

        /// <summary>
        /// Article approvement date
        /// </summary>
        [DataMember(Order = 9, IsRequired = true, Name = "approved")]
        public DateTime Approved { get; set; }

        /// <summary>
        /// Article group
        /// </summary>
        [DataMember(Order = 10, Name = "groupid")]
        public Guid GroupId { get; set; }

        #endregion

        
        #region Fields
        private Guid m_Id = Guid.NewGuid();
        private string mHead = default(string);
        private string mText = default(string);
        #endregion

        public override void AssignTo(object obj)
        {
            if (obj is Article)
            {
                var article = obj as Article;

                article.Id = Id;
                article.Approved = Approved;
                article.ArticleStatus = ArticleStatus;
                article.Author = Author;
                article.Content = Content;
                article.Moderator = Moderator;
                article.PictureUrl = PictureUrl;
                article.Title = Title;
                article.GroupId = GroupId;
                article.Updated = Updated;
                return;
            }
            throw new NotSupportedException();
        }

        public override bool IsKeyEquals( object key )
        {
            if (key is Article)
            {
                return Helpers.KeyHelper.IsGuidKeyEquals( key, (key as Article).Id );
            }

            return Helpers.KeyHelper.IsGuidKeyEquals( key, Id );
        }

        public bool Equals(Article other)
        {
            if (other == null)
                return false;

            if (!other.Id.Equals(Id))
                return false;

            if (!other.GroupId.Equals(GroupId))
                return false;

            if (other.ArticleStatus!= ArticleStatus)
                return false;


            if (string.Compare(other.Title, Title) != 0)
                return false;

            if (string.Compare(other.Content, Content) != 0)
                return false;

            if (string.Compare(other.PictureUrl, PictureUrl) != 0)
                return false;


            if (other.Updated.Subtract(Updated).Seconds > 1)
                return false;

            if (other.Approved.Subtract(Approved).Seconds > 1)
                return false;
            
            if (!other.Author.Key.Equals(Author.Key))
                return false;

            if (string.Compare(other.Author.Value, Author.Value) != 0)
                return false;

            if (!other.Moderator.Key.Equals(Moderator.Key))
                return false;

            if (string.Compare(other.Moderator.Value, Moderator.Value) != 0)
                return false;

            return true;
        }
    }
}