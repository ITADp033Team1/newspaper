﻿// <copyright file="Admin.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Bilokin Sviatoslav</author>
// <date>3/20/2013 4:12:26 PM</date>
// <summary>TODO: Update summary</summary>
namespace SoftServe.Modules.NewsPaperExtensions.Models
{
    using SoftServe.Modules.NewsPaperExtensions.Abstractions;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using System.Text;
    /// <summary>
    /// Administrator of application.
    /// </summary>
    [DataContract]
    public class AdminProfile : ProfileBase
    {
        #region Constructors

        static AdminProfile()
        {
            ProfileBase.RegisterType<AdminProfile>();
        }
        public AdminProfile()
        {
            Id = Guid.NewGuid();
            Login = "root";
        }

        public bool Equals(AdminProfile other)
        {
            if (other == null)
                return false;

            return base.Equals(other);
        }

        #endregion
    }
}
