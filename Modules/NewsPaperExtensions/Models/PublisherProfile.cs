﻿// <copyright file="PublisherProfile.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Bilokin Sviatoslav</author>
// <date>3/20/2013 4:03:53 PM</date>
// <summary>TODO: Update summary</summary>
namespace SoftServe.Modules.NewsPaperExtensions.Models
{
    using SoftServe.Modules.NewsPaperExtensions.Abstractions;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using System.Text;
    
    /// <summary>
    /// Publisher profile.
    /// </summary>
    [DataContract]
    public class PublisherProfile : UserProfile
    {
        static PublisherProfile()
        {
            ProfileBase.RegisterType<PublisherProfile>();
        }

        public bool Equals(PublisherProfile other)
        {
            if (other == null)
                return false;

            return base.Equals(other);
        }

    }
}
