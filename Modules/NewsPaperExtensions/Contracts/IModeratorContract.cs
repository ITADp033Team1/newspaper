﻿// <copyright file="IPublisherContract.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Kucher Inna</author>

namespace SoftServe.Modules.NewsPaperExtensions.Contracts
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using SoftServe.Modules.NewsPaperExtensions.Models;
    using SoftServe.Modules.NewsPaperExtensions.Abstractions;

        /// <summary>
        /// TODO: Update summary
        /// </summary>
        /// 
        public interface IModeratorContract : IDisposable
        {
            IEnumerable<PublishRequest> GetRequests(EntityFilterBase<PublishRequest> filter = null);

            /// <summary>
            /// Approves article
            /// </summary>
            /// <param name="articleId">Article's id</param>
            void ApproveArticle(Guid articleId);

            /// <summary>
            /// Declines article
            /// </summary>
            /// <param name="articleId">Article's id</param>
            /// <param name="comment">Comment to article. Required</param>
            void DeclineArticle(Guid articleId, string comment);
        }
    }
