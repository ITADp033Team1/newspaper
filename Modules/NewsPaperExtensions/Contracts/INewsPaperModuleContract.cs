/// -----------------------------------------------------------------------
/// <copyright file="IExtensionContract.cs" company="">
/// TODO: Update copyright text.
/// </copyright>
/// -----------------------------------------------------------------------

namespace SoftServe.Modules.NewsPaperExtensions.Contracts
{
    using System;

    /// <summary>
    /// Example extension contract
    /// </summary>
    public interface INewsPaperModuleContract : IAdminContract, IArticleContract, IModeratorContract, IDisposable
    {       
    }
}
