﻿// -----------------------------------------------------------------------
// <copyright file="NewspaperDbContext.cs" company="Soft Serve Academy">
// This code was written by Evgeniy Galenko
// </copyright>
// -----------------------------------------------------------------------
namespace SoftServe.Modules.NewsPaperExtensions.Data
{
    using SoftServe.Modules.NewsPaperExtensions.Entity_Models;
    using SoftServe.Modules.NewsPaperExtensions.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    // TODO: Add Article Statistics repository
    /// <summary>
    /// Represents db connection.
    /// </summary>
    public class NewsPaperDbContext : DbContext
    {
        static NewsPaperDbContext()
        {
            //Drops and creating database, when any model changed
            Database.SetInitializer<NewsPaperDbContext>
                (new DropCreateDatabaseIfModelChanges<NewsPaperDbContext>());
        }
        public DbSet<ArticleEntity> Articles { get; set; }
        public DbSet<ArticleStatisticsEntity> ArticleStatistics { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<ModeratorProfile> ModeratorProfiles { get; set; }
        public DbSet<PublisherProfile> PublisherProfiles { get; set; }
        public DbSet<PublishRequestEntity> PublishRequests { get; set; }
        public DbSet<PublishResponseEntity> PublishResponses { get; set; }
        public DbSet<SearchQuery> SearchQueries { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<AdminProfile> AdminProfiles { get; set; }
    }
}