﻿// <copyright file="KeyHelper.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Bilokin Sviatoslav</author>
// <date>3/22/2013 2:03:55 AM</date>
// <summary>TODO: Update summary</summary>
namespace SoftServe.Modules.NewsPaperExtensions.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    /// <summary>
    /// TODO: Update summary
    /// </summary>
    internal static class KeyHelper
    {

        public static bool IsGuidKeyEquals(object key, Guid originalKey)
        {
            if (key is Guid)
                return originalKey.CompareTo((Guid)key) == 0;

            if (key is string)
            {
                var gResult = default(Guid);
                if (!Guid.TryParse((string)key, out gResult))
                    return false;

                return originalKey.CompareTo(gResult) == 0;
            }
            return false;
        }

    }
}
