﻿// -----------------------------------------------------------------------
// <copyright file="ArticleEntity.cs" company="Soft Serve Academy">
// This code was written by Evgeniy Galenko
// </copyright>
// -----------------------------------------------------------------------
namespace SoftServe.Modules.NewsPaperExtensions.Entity_Models
{
    using Ocorola.Entity.Infrastructure;
    using SoftServe.Modules.NewsPaperExtensions.Abstractions;    
    using SoftServe.Modules.NewsPaperExtensions.Models;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Represents article database entity model
    /// </summary>
    public class ArticleEntity : IStorageEntity, IDbEntity
    {
        #region Properties
        /// <summary>
        /// Article  Id.
        /// </summary>
        [Key]
        [DataMember(Order = 1, IsRequired = true)]
        public Guid Id { get; set; }
        /// <summary>
        /// Article author id.
        /// </summary>
        [DataMember(Order = 2, IsRequired = true)]
        public Guid AuthorId
        {
            get;
            set;
        }
        /// <summary>
        /// Article author name
        /// </summary>
        [DataMember(Order = 3, IsRequired = true)]
        public string AuthorName
        {
            get;
            set;
        }
        /// <summary>
        /// Article head.
        /// </summary>
        [DataMember(Order = 4, IsRequired = true, Name = "subject")]
        public string Title
        {
            get { return mHead; }
            set { mHead = value; }
        }
        /// <summary>
        /// Article content.
        /// </summary>
        [DataMember(Order = 5, IsRequired = true, Name = "body")]
        public string Content
        {
            get { return mText; }
            set { mText = value; }
        }
        /// <summary>
        /// Status of article.
        /// </summary>
        [DataMember(Order = 6, Name = "status")]
        public eArticleStatus ArticleStatus { get; set; }

        /// <summary>
        /// Picture below the head of article.
        /// </summary>        
        [DataMember(Order = 7, Name = "imageUrl")]
        public string PictureUrl { get; set; }

        /// <summary>
        /// Name of article's moderator.
        /// </summary>

        [DataMember(Order = 8, Name = "moderatorId")]
        public Guid ModeratorId { get; set; }

        /// <summary>
        /// Moderator name
        /// </summary>
        [DataMember(Order = 9, Name = "moderatorName")]
        public string ModeratorName { get; set; }

        /// <summary>
        /// Article creation date.
        /// </summary>
        [DataMember(Order = 10, IsRequired = true, Name = "created")]
        public DateTime Updated { get; set; }

        /// <summary>
        /// Article approvement date
        /// </summary>
        [DataMember(Order = 11, IsRequired = true, Name = "approved")]
        public DateTime Approved { get; set; }
        #endregion

        #region Fields
        private string mHead = default(string);
        private string mText = default(string);
        #endregion

        public bool IsKeyEquals(object key)
        {
            return Helpers.KeyHelper.IsGuidKeyEquals(key, Id);
        }

        public void AssignTo(object obj)
        {
            if (obj is ArticleEntity)
            {
                var article = obj as ArticleEntity;

                article.Id = Id;
                article.Approved = Approved;
                article.ArticleStatus = ArticleStatus;
                article.AuthorId = AuthorId;
                article.AuthorName = AuthorName;
                article.Content = Content;
                article.ModeratorId = ModeratorId;
                article.ModeratorName = ModeratorName;
                article.PictureUrl = PictureUrl;
                article.Title = Title;
                article.Updated = Updated;
                return;
            }
            throw new NotSupportedException();
        }
    }
}