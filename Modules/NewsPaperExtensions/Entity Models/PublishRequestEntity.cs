﻿// -----------------------------------------------------------------------
// <copyright file="PublishRequestEntity.cs" company="Soft Serve Academy">
// This code was written by Evgeniy Galenko
// </copyright>
// -----------------------------------------------------------------------
namespace SoftServe.Modules.NewsPaperExtensions.Entity_Models
{
    using Ocorola.Entity.Infrastructure;
    using SoftServe.Modules.NewsPaperExtensions.Abstractions;
    using SoftServe.Modules.NewsPaperExtensions.Models;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Represents publish request database entity
    /// </summary>
    [DataContract()]
    public class PublishRequestEntity : IDbEntity
    {
        #region Properties
        /// <summary>
        /// Request ID.
        /// </summary>
        [Key]
        [DataMember(Order = 1, Name = "id")]
        public Guid Id
        {
            get;
            set;
        }

        /// <summary>
        /// Article ID.
        /// </summary>
        [DataMember(Order = 2, Name = "ArticleId")]
        public Guid ArticleId { get; set; }

        /// <summary>
        /// Article Info
        /// </summary>
        [DataMember(Order = 3, Name = "ArticleInfo")]
        public string ArticleInfo { get; set; }

        /// <summary>
        /// Publiser Id 
        /// </summary>
        [DataMember(Order = 4, Name = "PublisherId")]
        public Guid PublisherId { get; set; }

        /// <summary>
        /// Publiser Info
        /// </summary>
        [DataMember(Order = 5, Name = "PublisherIfno")]
        public string PublisherInfo { get; set; }

        /// <summary>
        /// Date of creation.
        /// </summary>
        [DataMember(Order = 6, Name = "date")]
        public DateTime Date { get; set; }

        /// <summary>
        /// Header of request.
        /// </summary>
        [DataMember(Order = 7, IsRequired = true, Name = "subject")]
        public string Header { get; set; }

        /// <summary>
        /// Tags of article with ArticleID.
        /// </summary>
        [DataMember(Order = 8, IsRequired = true, Name = "tags")]
        public string Tags { get; set; }
        #endregion

        public void AssignTo(object obj)
        {
            if (obj is PublishRequestEntity)
            {
                var publishRequest = obj as PublishRequestEntity;
                publishRequest.Id = Id;
                publishRequest.ArticleId = ArticleId;
                publishRequest.ArticleInfo = ArticleInfo;
                publishRequest.Date = Date;
                publishRequest.Header = Header;
                publishRequest.PublisherId = PublisherId;
                publishRequest.PublisherInfo = PublisherInfo;
                publishRequest.Tags = Tags;                

                return;
            }
            throw new NotSupportedException();
        }

        public bool IsKeyEquals(object key)
        {
            return Helpers.KeyHelper.IsGuidKeyEquals(key, ArticleId);
        }
    }
}