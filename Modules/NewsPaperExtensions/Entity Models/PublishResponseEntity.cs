﻿// -----------------------------------------------------------------------
// <copyright file="PublishResponseEntity.cs" company="Soft Serve Academy">
// This code was written by Evgeniy Galenko
// </copyright>
// -----------------------------------------------------------------------
namespace SoftServe.Modules.NewsPaperExtensions.Entity_Models
{
    using SoftServe.Modules.NewsPaperExtensions.Abstractions;
    using SoftServe.Modules.NewsPaperExtensions.Models;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Represents publish response database entity
    /// </summary>
    public class PublishResponseEntity: IDbEntity
    {
        #region Properties

        /// <summary>
        /// Responce ID.
        /// </summary>
        [Key]
        [DataMember(Order = 1, Name = "id")]
        public Guid Id
        {
            get;
            set;
        }
        /// <summary>
        /// Article ID.
        /// </summary>
        [DataMember(Order = 2, Name = "articleId")]
        public Guid ArticleId { get; set; }

        /// <summary>
        /// Article Info
        /// </summary>
        [DataMember(Order = 3, Name = "articleInfo")]
        public string ArticleInfo { get; set; }

        /// <summary>
        /// Publiser ID of article with ArticleID.
        /// </summary>
        [DataMember(Order = 4, IsRequired = true, Name = "publisherId")]
        public Guid PublisherId { get; set; }

        /// <summary>
        /// article with ArticleID.
        /// </summary>
        [DataMember(Order = 5, IsRequired = true, Name = "publisherInfo")]
        public string PublisherInfo { get; set; }

        /// <summary>
        /// Status which should be applied to the article with ArticleID.
        /// </summary>
        [DataMember(Order = 6, Name = "status")]
        [EnumDataType(typeof(eArticleStatus))]
        public eArticleStatus ChangedStatus { get; set; }
        #endregion

        public bool IsKeyEquals(object key)
        {
            return Helpers.KeyHelper.IsGuidKeyEquals(key, Id);
        }

        public void AssignTo(object obj)
        {
            if (obj is PublishResponseEntity)
            {
                var publishResponse = obj as PublishResponseEntity;

                publishResponse.Id = Id;
                publishResponse.ArticleId = ArticleId;
                publishResponse.ArticleInfo = ArticleInfo;
                publishResponse.ChangedStatus = ChangedStatus;
                publishResponse.PublisherId = PublisherId;
                publishResponse.PublisherInfo = PublisherInfo;                

                return;
            }
            throw new NotSupportedException();
        }
    }
}