﻿// -----------------------------------------------------------------------
// <copyright file="ArticleStatisticsEntity.cs" company="Soft Serve Academy">
// This code was written by Evgeniy Galenko
// </copyright>
// -----------------------------------------------------------------------
namespace SoftServe.Modules.NewsPaperExtensions.Entity_Models
{
    using Ocorola.Entity.Infrastructure;
    using SoftServe.Modules.NewsPaperExtensions.Abstractions;
    using SoftServe.Modules.NewsPaperExtensions.Models;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Represents article statistics database entity
    /// </summary>
    [DataContract()]
    public class ArticleStatisticsEntity: IStorageEntity, IDbEntity
    {
        public ArticleStatisticsEntity()
        {
            Id = Guid.NewGuid();
        }

        #region Properties
        [Key]
        [DataMember(Order = 0, Name = "ArticelStatisticsId")]
        public Guid Id { get; set; }

        [DataMember(Order = 1, Name = "id of article", IsRequired = true)]
        public Guid ArticleId { get; set; }

        [DataMember(Order = 2, Name = "number of views")]
        public int NumberOfViews { get; set; }

        [DataMember(Order = 3, Name = "article status")]
        [EnumDataType(typeof(eArticleStatus))]
        public eArticleStatus ArticleStatus { get; set; }

        [DataMember(Order = 4, Name = "words count")]
        public int WordsCount { get; set; }

        [DataMember(Order = 5, Name = "pages count")]
        public int PagesCount { get; set; }

        [DataMember(Order = 6, Name = "symbols count")]
        public int SymbolsCount { get; set; }
        #endregion

        public bool IsKeyEquals(object key)
        {
            return Helpers.KeyHelper.IsGuidKeyEquals(key, Id);
        }

        public void AssignTo(object obj)
        {
            if (obj is ArticleEntity)
            {
                var articleStatistics = obj as ArticleStatisticsEntity;

                articleStatistics.Id = Id;
                articleStatistics.ArticleId = ArticleId;
                articleStatistics.ArticleStatus = ArticleStatus;
                articleStatistics.NumberOfViews = NumberOfViews;
                articleStatistics.PagesCount = PagesCount;
                articleStatistics.SymbolsCount = SymbolsCount;
                articleStatistics.WordsCount = WordsCount;
            
                return;
            }
            throw new NotSupportedException();
        }
    }
}