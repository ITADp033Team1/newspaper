﻿// -----------------------------------------------------------------------
// <copyright file="Converters.cs" company="Soft Serve Academy">
// This code was written by Evgeniy Galenko
// </copyright>
// -----------------------------------------------------------------------
namespace SoftServe.Modules.NewsPaperExtensions.Entity_Models
{
    using AutoMapper;
    using SoftServe.Modules.NewsPaperExtensions.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Represents converter which converts entity types to model types
    /// </summary>
    public class ArticleEntityConverter : TypeConverter<ArticleEntity, Article>
    {
        static ArticleEntityConverter()
        {
            Mapper.CreateMap<Article, ArticleEntity>()
                .ForMember(ae => ae.ModeratorId, a => a.MapFrom<Guid>(s => s.Moderator.Key))
                .ForMember(ae => ae.ModeratorName, a => a.MapFrom<string>(s => s.Moderator.Value))
                .ForMember(ae => ae.AuthorId, a => a.MapFrom<Guid>(s => s.Author.Key))
                .ForMember(ae => ae.AuthorName, a => a.MapFrom<string>(s => s.Author.Value));                

        }
        /// <summary>
        /// Converts entity to the model
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        protected override Article ConvertCore(ArticleEntity entity)
        {
            if (entity == null)
                return null;

            return new Article()
            {
                Id = entity.Id,
                Approved = entity.Approved,
                ArticleStatus = entity.ArticleStatus,
                Author = new KeyValuePair<Guid, string>(entity.AuthorId, entity.AuthorName),
                Content = entity.Content,
                Moderator = new KeyValuePair<Guid, string>(entity.ModeratorId, entity.ModeratorName),
                PictureUrl = entity.PictureUrl,
                Updated = entity.Updated,
                Title = entity.Title
            };
        }
    }

    /// <summary>
    /// Represents converter which converts entity types to model types
    /// </summary>
    public class ArticleStatisticsEntityConverter : TypeConverter<ArticleStatisticsEntity, ArticleStatistics>
    {
        static ArticleStatisticsEntityConverter()
        {
            Mapper.CreateMap<ArticleStatistics, ArticleStatisticsEntity>()
                .ForMember(x => x.Id, y => y.MapFrom(z => Guid.NewGuid()));

        }
        protected override ArticleStatistics ConvertCore(ArticleStatisticsEntity entity)
        {
            if (entity == null)
                return null;
            return new ArticleStatistics()
            {
                ArticleId = entity.ArticleId,
                WordsCount = entity.WordsCount,
                SymbolsCount = entity.SymbolsCount,
                PagesCount = entity.PagesCount,
                NumberOfViews = entity.NumberOfViews,
                ArticleStatus = entity.ArticleStatus
            };
        }
    }

    /// <summary>
    /// Represents converter which converts entity types to model types
    /// </summary>
    public class PublishRequestEntityConverter : TypeConverter<PublishRequestEntity, PublishRequest>
    {
        static PublishRequestEntityConverter()
        {
            Mapper.CreateMap<PublishRequest, PublishRequestEntity>()
               .ForMember(ae => ae.ArticleId, a => a.MapFrom<Guid>(s => s.ArticleInfo.Key))
               .ForMember(ae => ae.ArticleInfo, a => a.MapFrom<string>(s => s.ArticleInfo.Value))
               .ForMember(ae => ae.PublisherId, a => a.MapFrom<Guid>(s => s.PublisherInfo.Key))
               .ForMember(ae => ae.PublisherInfo, a => a.MapFrom<string>(s => s.PublisherInfo.Value));
        }
        protected override PublishRequest ConvertCore(PublishRequestEntity entity)
        {
            if (entity == null)
                return null;

            return new PublishRequest()
            {
                Id = entity.Id,
                Header = entity.Header,
                PublisherInfo = new KeyValuePair<Guid, string>(entity.PublisherId, entity.PublisherInfo),
                ArticleInfo = new KeyValuePair<Guid, string>(entity.ArticleId, entity.ArticleInfo),
                Date = entity.Date,
                Tags = entity.Tags
            };
        }
    }

    /// <summary>
    /// Represents converter which converts entity types to model types
    /// </summary>
    public class PublishResponseEntityConverter : TypeConverter<PublishResponseEntity, PublishResponce>
    {
        static PublishResponseEntityConverter()
        {
            Mapper.CreateMap<PublishResponce, PublishResponseEntity>()
               .ForMember(ae => ae.ArticleId, a => a.MapFrom<Guid>(s => s.ArticleInfo.Key))
               .ForMember(ae => ae.ArticleInfo, a => a.MapFrom<string>(s => s.ArticleInfo.Value))
               .ForMember(ae => ae.PublisherId, a => a.MapFrom<Guid>(s => s.PubliserInfo.Key))
               .ForMember(ae => ae.PublisherInfo, a => a.MapFrom<string>(s => s.PubliserInfo.Value));
        }
        protected override PublishResponce ConvertCore(PublishResponseEntity entity)
        {
            if (entity == null)
                return null;

            return new PublishResponce()
            {
                Id = entity.Id,
                ChangedStatus = entity.ChangedStatus,
                ArticleInfo = new KeyValuePair<Guid, string>(entity.ArticleId, entity.ArticleInfo),
                PubliserInfo = new KeyValuePair<Guid, string>(entity.PublisherId, entity.PublisherInfo)
            };
        }
    }
}